
from cal_points import loadModel, runPAF
import time

if __name__ == '__main__':

    print("in test.py")
    tic = time.time()

    model = loadModel()
    print(model)
    runPAF(model)

    toc = time.time()
    print ('processing time is %.5f' % (toc - tic))

