import os
import sys
import argparse
import cv2
import math
import time
from datetime import datetime
import numpy as np
import tensorflow as tf

sys.path.append("/home/mrzhou/git/server_python/mysite/PAFs")

import util
import json
from config_reader import config_reader
from scipy.ndimage.filters import gaussian_filter

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from model.cmu_model import get_testing_model

# find connection in the specified sequence, center 29 is in the position 15
limbSeq = [[2, 3], [2, 6], [3, 4], [4, 5], [6, 7], [7, 8], [2, 9], [9, 10], \
           [10, 11], [2, 12], [12, 13], [13, 14], [2, 1], [1, 15], [15, 17], \
           [1, 16], [16, 18], [3, 17], [6, 18]]

# the middle joints heatmap correpondence
mapIdx = [[31, 32], [39, 40], [33, 34], [35, 36], [41, 42], [43, 44], [19, 20], [21, 22], \
          [23, 24], [25, 26], [27, 28], [29, 30], [47, 48], [49, 50], [53, 54], [51, 52], \
          [55, 56], [37, 38], [45, 46]]

# visualize
colors = [[255, 0, 0], [255, 85, 0], [255, 170, 0], [255, 255, 0], [170, 255, 0], [85, 255, 0],
          [0, 255, 0], \
          [0, 255, 85], [0, 255, 170], [0, 255, 255], [0, 170, 255], [0, 85, 255], [0, 0, 255],
          [85, 0, 255], \
          [170, 0, 255], [255, 0, 255], [255, 0, 170], [255, 0, 85]]

# [a, b] => point from a to b
bodyConnect = [[-1,1], [1,0], [1,2], [2,3], [3,4], [1,5], [5,6], [6,7], [-1,8], [8,9], [9,10], [-1,11], [11,12], [12,13]]

# body part need to adjust because of the relation
bodyRelate = [ [0], [0,1,2,3,4,5,6,7], [2,3,4], [3,4], [4], [5,6,7], [6,7], [7], [8,9,10], [9,10], [10], [11,12,13], [12,13], [13] ]


# save the body length of each body part
# body_len[8] = low_dis
body_len = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

resize_x = 640  # 288
resize_y = 480  # 384
zoom = 0.5

def bodyPartNormalize(result):
    for i in range( len(body_len) ):
        start = bodyConnect[i][0]
        end = bodyConnect[i][1]
        tmp_body_len = 0
        if start == -1:
            tmp_body_len = calLength(result[3*end], result[3*end+1], result[3*end+2])
        else:
            tmp_body_len = calLength(result[3*end], result[3*end+1], result[3*end+2], result[3*start], result[3*start+1], result[3*start+2])

    # calculate body length
        if body_len[len(body_len)-1] == 0:
            if i == 5 or i == 6 or i == 7 or i == 11 or i == 12 or i == 13:
                body_len[i] = body_len[i-3]
                pass
            else:
                body_len[i] = tmp_body_len

    # adjust other frame
        else:
            if tmp_body_len !=0 :
                # vector between connection
                if start == -1:
                    vec_x = result[3*end]
                    vec_y = result[3*end+1]
                    vec_z = result[3*end+2]
                else:
                    vec_x = result[3*end] - result[3*start]
                    vec_y = result[3*end+1] - result[3*start+1]
                    vec_z = result[3*end+2] - result[3*start+2]

                # decrease value
                len_diff = tmp_body_len - body_len[i]
                param = len_diff/tmp_body_len
                vec_x = vec_x*param
                vec_y = vec_y*param
                vec_z = vec_z*param

                for j in range( len(bodyRelate[end]) ):
                    movement = bodyRelate[end][j]
                    result[3*movement] -= vec_x
                    result[3*movement+1] -= vec_y
                    result[3*movement+2] -= vec_z

def headAdjust(result):
    body_x = result[3]
    body_y = result[4]
    body_z = result[5]

    rhip_x = result[24]
    rhip_y = result[25]
    rhip_z = result[26]

    vec_A_x = body_y*rhip_z - rhip_y*body_z
    vec_A_y = body_z*rhip_x - rhip_z*body_x
    vec_A_z = body_x*rhip_y - rhip_x*body_y

    vec_len  = calLength( vec_A_x, vec_A_y, vec_A_z )
    vec_A_x /= vec_len
    vec_A_y /= vec_len
    vec_A_z /= vec_len

    vec_A_x *= body_len[8]
    vec_A_y *= body_len[8]
    vec_A_z *= body_len[8]

    result[0] -= vec_A_x
    result[1] -= vec_A_y
    result[2] -= vec_A_z

def calLength(p1_x, p1_y, p1_z, p2_x=0, p2_y=0, p2_z=0):
    x_diff = p1_x - p2_x
    y_diff = p1_y - p2_y
    z_diff = p1_z - p2_z
    answer = math.sqrt( math.pow(x_diff, 2) + math.pow(y_diff, 2) + math.pow(z_diff, 2))
    return answer

def calVector(p1_x, p1_y, p1_z, p2_x=0, p2_y=0, p2_z=0):
    x_diff = p1_x - p2_x
    y_diff = p1_y - p2_y
    z_diff = p1_z - p2_z
    answer = math.sqrt( math.pow(x_diff, 2) + math.pow(y_diff, 2) + math.pow(z_diff, 2))
    return answer, x_diff, y_diff, z_diff

def process (input_image, params, model_params, model):
    #(h,w) = input_image.shape[:2]
    # resize_x = input_image.width  # 288
    # resize_y = input_image.higet
    # print(resize_x, resize_y)
    #ln[6],ln[7]
    # oriImg_tmp = cv2.imread(input_image)  # B,G,R order
    # img_height, img_width, channels = oriImg_tmp.shape
    # zoom = 800/img_height
    # resize_x = int(img_width*zoom)  # 288
    # resize_y = int(img_height*zoom)
    # resize_x = 450  # 288
    # resize_y = 800
    # print(resize_x, resize_y)
    oriImg_tmp = cv2.imread(input_image)  # B,G,R order

    oriImg = cv2.resize(oriImg_tmp, (resize_x, resize_y))
    multiplier = [x * model_params['boxsize'] / oriImg.shape[0] for x in params['scale_search']]

    heatmap_avg = np.zeros((oriImg.shape[0], oriImg.shape[1], 19)) #oriImg.shape[0] = height, oriImg.shape[1] = width
    paf_avg = np.zeros((oriImg.shape[0], oriImg.shape[1], 38))

    for m in range(len(multiplier)): #len(multiplier)=4
        scale = multiplier[m]
        #outputSizr = Size(round(fx*src.cols), round(fy*src.rows))

        #imageToTest_padded                             width, height
        imageToTest = cv2.resize(oriImg, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)
        imageToTest_padded, pad = util.padRightDownCorner(imageToTest, model_params['stride'],
                                                          model_params['padValue'])

        input_img = np.transpose(np.float32(imageToTest_padded[:,:,:,np.newaxis]), (3,0,1,2)) # required shape (1, width, height, channels)

        output_blobs = model.predict(input_img)

        # extract outputs, resize, and remove padding
        ####  heatmap ####
        heatmap = np.squeeze(output_blobs[1])  # output 1 is heatmaps
        heatmap = cv2.resize(heatmap, (0, 0), fx=model_params['stride'], fy=model_params['stride'],
                             interpolation=cv2.INTER_CUBIC)
        heatmap = heatmap[:imageToTest_padded.shape[0] - pad[2], :imageToTest_padded.shape[1] - pad[3],
                  :]
        heatmap = cv2.resize(heatmap, (oriImg.shape[1], oriImg.shape[0]), interpolation=cv2.INTER_CUBIC)
        ####  paf ####
        paf = np.squeeze(output_blobs[0])  # output 0 is PAFs
        paf = cv2.resize(paf, (0, 0), fx=model_params['stride'], fy=model_params['stride'],
                         interpolation=cv2.INTER_CUBIC)
        paf = paf[:imageToTest_padded.shape[0] - pad[2], :imageToTest_padded.shape[1] - pad[3], :]
        paf = cv2.resize(paf, (oriImg.shape[1], oriImg.shape[0]), interpolation=cv2.INTER_CUBIC)

        heatmap_avg = heatmap_avg + heatmap / len(multiplier)
        paf_avg = paf_avg + paf / len(multiplier)

    all_peaks = []
    peak_counter = 0

    for part in range(18):
        map_ori = heatmap_avg[:, :, part]
        map = gaussian_filter(map_ori, sigma=3) #from scipy.ndimage.filters import gaussian_filter

        map_left = np.zeros(map.shape)
        map_left[1:, :] = map[:-1, :]
        map_right = np.zeros(map.shape)
        map_right[:-1, :] = map[1:, :]
        map_up = np.zeros(map.shape)
        map_up[:, 1:] = map[:, :-1]
        map_down = np.zeros(map.shape)
        map_down[:, :-1] = map[:, 1:]

        peaks_binary = np.logical_and.reduce(
            (map >= map_left, map >= map_right, map >= map_up, map >= map_down, map > params['thre1']))
        peaks = list(zip(np.nonzero(peaks_binary)[1], np.nonzero(peaks_binary)[0]))  # note reverse
        peaks_with_score = [x + (map_ori[x[1], x[0]],) for x in peaks]
        id = range(peak_counter, peak_counter + len(peaks))
        peaks_with_score_and_id = [peaks_with_score[i] + (id[i],) for i in range(len(id))]

        all_peaks.append(peaks_with_score_and_id)
        peak_counter += len(peaks)

    connection_all = []
    special_k = []
    mid_num = 10

    for k in range(len(mapIdx)):
        score_mid = paf_avg[:, :, [x - 19 for x in mapIdx[k]]]
        candA = all_peaks[limbSeq[k][0] - 1]    #Body PartA
        candB = all_peaks[limbSeq[k][1] - 1]    #Body PartB
        nA = len(candA)
        nB = len(candB)
        indexA, indexB = limbSeq[k]
        if (nA != 0 and nB != 0):
            connection_candidate = []
            for i in range(nA):         #len(candA)
                for j in range(nB):     #len(candB)
                    vec = np.subtract(candB[j][:2], candA[i][:2])
                    norm = math.sqrt(vec[0] * vec[0] + vec[1] * vec[1])
                    # failure case when 2 body parts overlaps
                    if norm == 0:
                        continue
                    vec = np.divide(vec, norm)

                    startend = list(zip(np.linspace(candA[i][0], candB[j][0], num=mid_num), \
                                   np.linspace(candA[i][1], candB[j][1], num=mid_num)))

                    vec_x = np.array(
                        [score_mid[int(round(startend[I][1])), int(round(startend[I][0])), 0] \
                         for I in range(len(startend))])
                    vec_y = np.array(
                        [score_mid[int(round(startend[I][1])), int(round(startend[I][0])), 1] \
                         for I in range(len(startend))])

                    score_midpts = np.multiply(vec_x, vec[0]) + np.multiply(vec_y, vec[1])
                    score_with_dist_prior = sum(score_midpts) / len(score_midpts) + min(
                        0.5 * oriImg.shape[0] / norm - 1, 0)
                    criterion1 = len(np.nonzero(score_midpts > params['thre2'])[0]) > 0.8 * len(
                        score_midpts)
                    criterion2 = score_with_dist_prior > 0
                    if criterion1 and criterion2:
                        connection_candidate.append([i, j, score_with_dist_prior,
                                                     score_with_dist_prior + candA[i][2] + candB[j][2]])

            connection_candidate = sorted(connection_candidate, key=lambda x: x[2], reverse=True)
            connection = np.zeros((0, 5))
            for c in range(len(connection_candidate)):
                i, j, s = connection_candidate[c][0:3]
                if (i not in connection[:, 3] and j not in connection[:, 4]):
                    connection = np.vstack([connection, [candA[i][3], candB[j][3], s, i, j]])
                    if (len(connection) >= min(nA, nB)):
                        break

            connection_all.append(connection)
        else:
            special_k.append(k)
            connection_all.append([])

    # last number in each row is the total parts number of that person
    # the second last number in each row is the score of the overall configuration
    subset = -1 * np.ones((0, 20))
    candidate = np.array([item for sublist in all_peaks for item in sublist])
    for k in range(len(mapIdx)):
        if k not in special_k:
            partAs = connection_all[k][:, 0]
            partBs = connection_all[k][:, 1]
            indexA, indexB = np.array(limbSeq[k]) - 1

            for i in range(len(connection_all[k])):  # = 1:size(temp,1)
                found = 0
                subset_idx = [-1, -1]
                for j in range(len(subset)):  # 1:size(subset,1):
                    if subset[j][indexA] == partAs[i] or subset[j][indexB] == partBs[i]:
                        subset_idx[found] = j
                        found += 1

                if found == 1:
                    j = subset_idx[0]
                    if (subset[j][indexB] != partBs[i]):
                        subset[j][indexB] = partBs[i]
                        subset[j][-1] += 1
                        subset[j][-2] += candidate[partBs[i].astype(int), 2] + connection_all[k][i][2]
                elif found == 2:  # if found 2 and disjoint, merge them
                    j1, j2 = subset_idx
                    membership = ((subset[j1] >= 0).astype(int) + (subset[j2] >= 0).astype(int))[:-2]
                    if len(np.nonzero(membership == 2)[0]) == 0:  # merge
                        subset[j1][:-2] += (subset[j2][:-2] + 1)
                        subset[j1][-2:] += subset[j2][-2:]
                        subset[j1][-2] += connection_all[k][i][2]
                        subset = np.delete(subset, j2, 0)
                    else:  # as like found == 1
                        subset[j1][indexB] = partBs[i]
                        subset[j1][-1] += 1
                        subset[j1][-2] += candidate[partBs[i].astype(int), 2] + connection_all[k][i][2]

                # if find no partA in the subset, create a new subset
                elif not found and k < 17:
                    row = -1 * np.ones(20)
                    row[indexA] = partAs[i]
                    row[indexB] = partBs[i]
                    row[-1] = 2
                    row[-2] = sum(candidate[connection_all[k][i, :2].astype(int), 2]) + \
                              connection_all[k][i][2]
                    subset = np.vstack([subset, row])

    # delete some rows of subset which has few parts occur
    deleteIdx = []
    for i in range(len(subset)):
        if subset[i][-1] < 4 or subset[i][-2] / subset[i][-1] < 0.4:
            deleteIdx.append(i)
    subset = np.delete(subset, deleteIdx, axis=0)

    #link all human's limbs
    stickwidth = 4
    limb_exist_list = np.zeros((len(subset), 17))       #[human][limb]
    part_coordinate_list = np.zeros((len(subset), 18, 2)) #[human][part][X,Y]
    for n in range(len(subset)):
        for i in range(17):
            index = subset[n][np.array(limbSeq[i])-1]
            if -1 in index:
                limb_exist_list[n][i] = 0
                part_coordinate_list[n][limbSeq[i][0]-1][0] = 0 # x_axis
                part_coordinate_list[n][limbSeq[i][1]-1][0] = 0 # x_axis
                part_coordinate_list[n][limbSeq[i][0]-1][1] = 0 # y_axis
                part_coordinate_list[n][limbSeq[i][1]-1][1] = 0 # y_axis
                continue
            limb_exist_list[n][i] = 1

            X = candidate[index.astype(int), 0]  # actually is x-axis
            Y = candidate[index.astype(int), 1]  # actually is y-axis
            #First limb
            part_coordinate_list[n][limbSeq[i][0]-1][0] = int(X[0]) # x_axis
            part_coordinate_list[n][limbSeq[i][0]-1][1] = int(Y[0]) # y_axis
            #Second limb
            part_coordinate_list[n][limbSeq[i][1]-1][0] = int(X[1]) # x_axiis
            part_coordinate_list[n][limbSeq[i][1]-1][1] = int(Y[1]) # y_axis


            mY = np.mean(Y)
            mX = np.mean(X)
            length = ((Y[0] - Y[1]) ** 2 + (X[0] - X[1]) ** 2) ** 0.5
            angle = math.degrees(math.atan2(Y[0] - Y[1], X[0] - X[1]))
            polygon = cv2.ellipse2Poly((int(mX),int(mY)), (int(length/2), stickwidth), int(angle), 0, 360, 1)


    #For seperate the image that can't be used for 3D_Rebuild
    #find out who has all limSeq[0]~limSeq[12]
    human_success_list = np.zeros(len(subset)) #[human]
    for n in range(len(limb_exist_list)):
        counter = 0
        for i in range(13):
            if limb_exist_list[n][i] == 1:
                counter+=1
        if counter == 13:
            human_success_list[n] = 1
        else:
            human_success_list[n] = 0

    #find the longest distance between two parts on Y_axis for each human
    distance_Y_axis = np.zeros(len(part_coordinate_list))   #[human]
    longest_A_B = np.zeros((len(part_coordinate_list), 2))  #[human][partA, partB]
    for i in range(len(part_coordinate_list)):
        for j in range(14):
            for k in range(j+1,14):
                distance_tmp = abs(part_coordinate_list[0][j][1] - part_coordinate_list[0][k][1])
                if distance_tmp > distance_Y_axis[i] :
                    distance_Y_axis[i] = distance_tmp
                    longest_A_B[i][0] = j
                    longest_A_B[i][1] = k

    return part_coordinate_list, longest_A_B, distance_Y_axis, human_success_list


disappear_point_dis = [0,0] # 0 => left, 1 => right
scale_param = [0,0]

def process_loop (input_image, image_number, params, model_params, model, frame):
    scale_param = [1,1]
    part_coordinate_list = [[]]*image_number
    longest_A_B = [[]]*image_number
    distance_Y_axis = [[]]*image_number
    human_success_list = [[]]*image_number
    #-------------------process all images-------------------#
    for i in range(image_number):
        part_coordinate_list[i], longest_A_B[i], distance_Y_axis[i], human_success_list[i] =  process(input_image[i], params, model_params, model)

    #-------------------adjust 2D image position
    #part_coordinate_list[left/right][person][part][x,y]


    #-----------------normalize to same scale------------------#
    base_point = int(longest_A_B[0][0][0])
    print(distance_Y_axis)
    relative_rate = float(distance_Y_axis[1][0]/distance_Y_axis[0][0])
    for i in range(14):
        part_coordinate_list[1][0][i][1] = part_coordinate_list[0][0][i][1] #y-axis
        delta_x = part_coordinate_list[1][0][i][0] - part_coordinate_list[1][0][base_point][0]
        variation = delta_x * relative_rate
        part_coordinate_list[1][0][i][0] = part_coordinate_list[1][0][base_point][0] + variation

    #------------calculate the point according to picture center------------#

    origin_shift_x = resize_x * 0.5
    origin_shift_y = resize_y * 0.5

    for i in range(2):
        for j in range(14):
            part_coordinate_list[i][0][j][0] -= origin_shift_x
            part_coordinate_list[i][0][j][1] -= origin_shift_y


    '''

    # calculate the adjustment

    # adjust the picture
    #print(disappear_point_dis, scale_param)
    for image in range(image_number):
        for part in range(14):
            dis_from_center = abs(resize_x/2 - part_coordinate_list[image][0][part][0])
            half_dis_from_center = dis_from_center/2
            sin_theta = half_dis_from_center/disappear_point_dis[1]
            if sin_theta>1:
                theta = math.pi
                pass
            else:
                theta = math.asin(half_dis_from_center/disappear_point_dis[1])*2
            adjustment = disappear_point_dis[image]*math.sin(theta)*scale_param[image]


            # theta = math.atan2(dis_from_center, disappear_point_dis[image])
            # print(dis_from_center, disappear_point_dis[image])

            #print(theta)
            #adjustment = disappear_point_dis[image] * theta

            # dis_from_center_sqr = dis_from_center*dis_from_center
            # disappear_point_dis_sqr = disappear_point_dis[image]*disappear_point_dis[image]
            # adjustment = dis_from_center - math.sqrt(dis_from_center_sqr*disappear_point_dis_sqr/(dis_from_center_sqr+disappear_point_dis_sqr))

            #print("before adjust: ", part_coordinate_list[image][0][part][0])

            # camera_choose = 0
            # if image == 0:
            #     camera_choose = 1
            #     pass

            # judge if the part is in front of body or center

            if part_coordinate_list[image][0][part][0] > resize_x/2 :
                adjustment = -adjustment
                pass

            # print("before adjust: ", part_coordinate_list[image][0][part][0])
            # print("adjustment: ", adjustment)

            #part_coordinate_list[image][0][part][0] = part_coordinate_list[image][0][part][0]+adjustment
            part_coordinate_list[image][0][part][0] = resize_x/2 - adjustment

            # print("after adjust: ", part_coordinate_list[image][0][part][0])

            pass
    print(frame)

    '''

    #------------------initial process for points------------------#
    result = []
    for i in range(14):
        result.append(part_coordinate_list[1][0][i][0]/10)  #x-axis
        result.append(part_coordinate_list[0][0][i][0]/10)  #y-axis
        result.append(-part_coordinate_list[1][0][i][1]/10) #z-axis

    #-------------------Adjust the body to be balanced-------------------#

    right_to_left, right_to_left_x, right_to_left_y, right_to_left_z = calVector(result[33],result[34],result[35],result[6],result[7],result[8])
    left_to_right, left_to_right_x, left_to_right_y, left_to_right_z = calVector(result[24],result[25],result[26],result[15],result[16],result[17])

    Center_point = []
    unit_vector = []
    if right_to_left >= left_to_right :
        Center_point.append((result[24] + result[15])/2)
        Center_point.append((result[25] + result[16])/2)
        Center_point.append((result[26] + result[17])/2)
        unit_vector.append(left_to_right_x/left_to_right)
        unit_vector.append(left_to_right_y/left_to_right)
        unit_vector.append(left_to_right_z/left_to_right)

        result[24] = Center_point[0]+unit_vector[0]*(left_to_right/2)
        result[25] = Center_point[1]+unit_vector[1]*(left_to_right/2)
        result[26] = Center_point[2]+unit_vector[2]*(left_to_right/2)
        result[15] = Center_point[0]-unit_vector[0]*(left_to_right/2)
        result[16] = Center_point[1]-unit_vector[1]*(left_to_right/2)
        result[17] = Center_point[2]-unit_vector[2]*(left_to_right/2)
    else:
        Center_point.append((result[33] + result[6])/2)
        Center_point.append((result[34] + result[7])/2)
        Center_point.append((result[35] + result[8])/2)
        unit_vector.append(right_to_left_x/right_to_left)
        unit_vector.append(right_to_left_y/right_to_left)
        unit_vector.append(right_to_left_z/right_to_left)

        result[33] = Center_point[0]+unit_vector[0]*(right_to_left/2)
        result[34] = Center_point[1]+unit_vector[1]*(right_to_left/2)
        result[35] = Center_point[2]+unit_vector[2]*(right_to_left/2)
        result[6] = Center_point[0]-unit_vector[0]*(right_to_left/2)
        result[7] = Center_point[1]-unit_vector[1]*(right_to_left/2)
        result[8] = Center_point[2]-unit_vector[2]*(right_to_left/2)

    #------------calculate the point according to heapcenter------------#

    origin_shift_x = (result[24]+result[33])*0.5
    origin_shift_y = (result[25]+result[34])*0.5
    origin_shift_z = (result[26]+result[35])*0.5

    for j in range(14):
        result[ 3*j ] -= origin_shift_x
        result[3*j+1] -= origin_shift_y
        result[3*j+2] -= origin_shift_z

    bodyPartNormalize(result)

    #-----------cal head position-----------#
    headAdjust(result)

    for i in range(42):
        result[i] = round( result[i], 3)


    return result

def adjustPoint(model, graph):
    adjust_image_path = '/home/mrzhou/Pictures/data_image/Adjust/'

    adjust_image_list = os.listdir(adjust_image_path)
    adjust_image_list.sort()

    print(adjust_image_list)
    params, model_params = config_reader()


    for frame in range(2):
    #for frame in range(1):
        input_image_1 = adjust_image_path + adjust_image_list[frame]
        input_image_2 = adjust_image_path + adjust_image_list[frame+2]

        input_image = []
        input_image.append(input_image_1)
        input_image.append(input_image_2)
        image_number = len(input_image)
        part_coordinate_list = [[]]*image_number
        longest_A_B = [[]]*image_number
        distance_Y_axis = [[]]*image_number
        human_success_list = [[]]*image_number

        for i in range(image_number):
            with graph.as_default():
                part_coordinate_list[i], longest_A_B[i], distance_Y_axis[i], human_success_list[i] =  process(input_image[i], params, model_params, model)

        #-----------------normalize to same scale------------------#
        base_point = int(longest_A_B[0][0][0])
        relative_rate = float(distance_Y_axis[1][0]/distance_Y_axis[0][0])
        for i in range(14):
            part_coordinate_list[1][0][i][1] = part_coordinate_list[0][0][i][1] #y-axis
            delta_x = part_coordinate_list[1][0][i][0] - part_coordinate_list[1][0][base_point][0]
            variation = delta_x * relative_rate
            part_coordinate_list[1][0][i][0] = part_coordinate_list[1][0][base_point][0] + variation


        # calculate the adjustment
        if frame == 0:
            # get right arm length, from right cam
            rArm_length = abs(part_coordinate_list[1][0][2][0] - part_coordinate_list[1][0][4][0])
            # get right arm shifting, from left cam
            rArm_shifting = abs(part_coordinate_list[0][0][2][0] - part_coordinate_list[0][0][4][0])

            # hand from center
            dis_from_center = abs(part_coordinate_list[0][0][4][0]-resize_x/2)

            half_dis_from_center = dis_from_center/2


            # calculate the disappear point distance
            '''
            disappear_point_dis[0] = (dis_from_center-rArm_shifting)/math.sqrt(2*rArm_shifting/dis_from_center-rArm_shifting*rArm_shifting/dis_from_center/dis_from_center)

            '''
            relate_param =  dis_from_center / rArm_shifting
            disappear_point_dis[0] = rArm_length * relate_param
            sin_theta = half_dis_from_center/disappear_point_dis[0]
            print(dis_from_center, rArm_shifting)
            print(half_dis_from_center)
            if sin_theta>1:
                theta = math.pi
                pass
            else:
                theta = math.asin(sin_theta)*2
            print(theta)
            scale_param[0] = 1 - rArm_shifting/disappear_point_dis[0]*math.sin(theta)
            print("left camera correct")
            pass

        elif frame == 1:

            # get left arm length, from left cam
            lArm_length = abs(part_coordinate_list[0][0][5][0] - part_coordinate_list[0][0][7][0])
            # get left arm shifting, from right cam
            lArm_shifting = abs(part_coordinate_list[1][0][5][0] - part_coordinate_list[1][0][7][0])
            # hand from center
            dis_from_center = abs(part_coordinate_list[1][0][7][0]-resize_x/2)

            half_dis_from_center = dis_from_center/2

            # calculate the disappear point distance
            '''
            disappear_point_dis[1] = (dis_from_center-lArm_shifting)/math.sqrt(2*lArm_shifting/dis_from_center-lArm_shifting*lArm_shifting/dis_from_center/dis_from_center)
            '''
            relate_param =  dis_from_center / lArm_shifting
            disappear_point_dis[1] = lArm_length * relate_param
            theta = math.asin(half_dis_from_center/disappear_point_dis[1])*2
            scale_param[1] = 1 - lArm_shifting/disappear_point_dis[1]*math.sin(theta)
            print("right camera correct")
            print(scale_param)

    pass

def loadModel():

    print('start processing...')
    weights_path = '/home/mrzhou/git/server_python/mysite/PAFs/model/keras/model.h5'
    print("finished weight_path")
    #ln[3]
    # load model
    # authors of original model don't use
    # vgg normalization (subtracting mean) on input images

    model = get_testing_model()     #at cmu_model.py
    print("finished get_test_model")
    model.load_weights(weights_path)
    graph = tf.get_default_graph()
    return model, graph

def runPAF(model, graph):
    adjustPoint(model, graph)
    first_image_path = '/home/mrzhou/Pictures/data_image/first_image/'
    second_image_path = '/home/mrzhou/Pictures/data_image/second_image/'

    first_image_list = os.listdir(first_image_path)
    second_image_list = os.listdir(second_image_path)
    first_image_list.sort()
    second_image_list.sort()

    print(first_image_list)
    print(second_image_list)
    data = {}

    params, model_params = config_reader()

    timestr = str(datetime.now())
    time = timestr.split(".")
    time.pop()
    data["filename"] = time[0]
    data["points"] = 14
    data["frame"] = len(first_image_list)
    data["len_pre"] = 0
    for frame in range(len(first_image_list)):
    #for frame in range(5):
        input_image_1 = first_image_path + first_image_list[frame]
        input_image_2 = second_image_path + second_image_list[frame]

        input_image = []
        input_image.append(input_image_1)
        input_image.append(input_image_2)
        image_number = len(input_image)
        '''
        print("\nDeal with image_1")
        print(first_image_list[frame])
        print("\nDeal with image_2")
        print(second_image_list[frame])
        '''
        with graph.as_default():
            result = process_loop(input_image, image_number, params, model_params, model, frame)
        '''
        if success_first != 1 || success_second != 1:
            print("Pass this frame\n")
            continue
        print("Finish this frame\n")
        '''
        data[frame] = result
        if frame == 0:
            data["len_pre"] = round( body_len[8] , 4)
        #print(result)

    with open("/home/mrzhou/git/server_python/mysite/Data/"+str(data["filename"])+".json", "w") as f:
        json.dump(data, f, indent=2, separators=(',', ': ') )
        f.close()

if __name__ == '__main__':
    os.system("pip show tensorflow-gpu")
    tic = time.time()

    model, graph = loadModel()
    print(model)
    runPAF(model, graph)

    toc = time.time()
    print ('processing time is %.5f' % (toc - tic))


