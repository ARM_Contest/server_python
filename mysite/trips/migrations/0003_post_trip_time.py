# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trips', '0002_auto_20180804_1500'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='trip_time',
            field=models.CharField(max_length=100, blank=True),
        ),
    ]
