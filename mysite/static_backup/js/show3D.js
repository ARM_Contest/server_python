
//--------variable for basic scene
var container;
var camera, scene, renderer;
var group, person;
var dir_light;
var target_rotation = {x: 0, y: 0};
var target_rotation_on_mouse_down = {x: 0, y: 0};

var camera_dis_slope = -0.02247;
var camera_dis_shifting = 49;

//--------Variable for controll
var mouse_mouse_up = { x: 0, y: 0};
var mouse_mouse_down = { x: 0, y: 0};
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

var geometry = null;
var material = null;

var origin_shift = {x: 0,  y: 0,  z: 0}

var Quaternion = {
  w: 0,
  x: 0,
  y: 0,
  z: 0
}

var Vector = {
  x:0,
  y:0,
  z:0
}


var lowdis=0;


/*
//---------read body part position
var data = {
  "frame": 1,
  "points": 14,
  
  "1" : [
    0,   0, 65,
    0,   0, 50,
    0,  20, 50,  0,  26,  20,  0,  25,   0, 
    0, -20, 50,  0, -26,  20,  0, -25,   0,  
    0,  15,  0,  0,  20, -44,  0,  25, -84,
    0, -15,  0,  0, -20, -44,  0, -25, -84
  ],
  "2" : [
    0,   0, 65,
    0,   0, 50,
    0,  20, 50,  10,  26,  20, 20,  25,   5,
    0, -20, 50,  10, -26,  20, 20, -25,   5,  
    0,  15,  0,  30,  20, -30,  10,  25, -64,
    0, -15,  0,   5, -20, -44,  10, -25, -84
  ]
  "1" : [
    21.0, 22.9, -17.2,
    18.8, 24.0, -23.0, 
    16.4, 20.1, -22.5, 21.4, 15.0, -19.3, 26.0, 8.9, -14.7, 
    21.1, 27.9, -23.4, 25.5, 29.8, -28.3, 30.2, 32.1, -33.4, 
    19.2, 19.9, -35.3, 19.0, 20.3, -43.9, 18.4, 21.7, -51.4, 
    22.3, 24.9, -35.7, 20.6, 25.8, -44.6, 20.1, 25.2, -52.3
  ]
  "1" : [
    26.8, 21.5, -14.8, 
    23.2, 24.4, -21.6, 
    19.6, 20.4, -21.4, 19.5, 19.8, -28.3, 21.3, 18.0, -34.7, 
    26.7, 28.4, -21.8, 30.0, 28.5, -29.8, 31.8, 26.6, -23.4, 
    22.0, 20.6, -35.6, 21.1, 21.8, -43.8, 21.8, 22.8, -52.0, 
    26.6, 25.2, -36.2, 25.3, 25.9, -45.4, 24.2, 25.1, -53.7
  ]
  
  "0" : [
    21.0, 22.9, -17.2,
    18.8, 24.0, -23.0, 
    16.4, 20.1, -22.5, 21.4, 15.0, -19.3, 26.0, 8.9, -14.7, 
    21.1, 27.9, -23.4, 25.5, 29.8, -28.3, 30.2, 32.1, -33.4, 
    19.2, 19.9, -35.3, 19.0, 20.3, -43.9, 18.4, 21.7, -51.4, 
    22.3, 24.9, -35.7, 20.6, 25.8, -44.6, 20.1, 25.2, -52.3
  ]
}
*/

adjustPoint();

geometry = new THREE.SphereGeometry( 0.5, 50, 50 );
material = new THREE.MeshLambertMaterial( { color: 0xffffff, transparent: true} );
var nose = new THREE.Mesh( geometry, material );
nose.position.x = data[0][0];
nose.position.z = -data[0][1];
nose.position.y = data[0][2];



lowdis = calLength(data[0][24], data[0][25], data[0][26], data[0][33], data[0][34], data[0][35])/2; //right hip to left hip


for (var i=0; i<data.frame; i++) {
  calHeadPos(i);
}


//var requestURL = './data.json';
//var file = './data.json';
//var data = document.open(file);
//var contents = data.document.body.innerText; 
/*
var data = $.getJSON('./data.json', function () {
    console.log("success");
})

var request = new XMLHttpRequest();
request.open('GET', requestURL);
request.responseType = 'json';
request.send();
var data = request.response;
*/


//----------Setup each body part color 
var body_part_color = [ 0x28ff28, 0x00ffff,           //hand, body
                        0xb766ad, 0xff0000, 0xffd306, //Right hand
                        0xb766ad, 0xff0000, 0xffd306, //Left hand
                        0xb766ad, 0xff0000, 0xffd306, //Right leg
                        0xb766ad, 0xff0000, 0xffd306 ]//Left leg

/*
  number  =>  Body Part         =>  node 

      0   =>  Neck              =>  Nose
      1   =>  Body              =>  Neck

      2   =>  Right Arm         =>  Right Shoulder 
      3   =>  Right Forearm     =>  Right Elbow
      4   =>  Right hand        =>  Right Wrist

      5   =>  Left Arm          =>  Left Shoulder 
      6   =>  Left Forearm      =>  Left Elbow
      7   =>  Left hand         =>  Left Wrist

      8   =>  Right Thigh       =>  Right Hip
      9   =>  Right Calf        =>  Right Knee
     10   =>  Right Foot        =>  Right Ankle

     11   =>  Left Thigh        =>  Left Hip
     12   =>  Left Calf         =>  Left Knee
     13   =>  Left Foot         =>  Left Ankle
*/

var bodyNodeObj = {
  id: 0,
  object: null,
  top_rad: 0,
  low_rad: 0,
  vec: 0,             // if the body part has derection, set to 1
  p1: 0,              // the point the body part connect
  p2: 0,

  create: function( input_group ){
    input_group.add( this.object );
  },
  setPosition: function(start_x, start_y, start_z, end_x=0, end_y=0, end_z=0) {       //use unit vector
    if (  this.vec == 0 ) {
      this.object.position.x = start_x;
      this.object.position.z = start_y;
      this.object.position.y = start_z;
    }
  },
  setId: function( num ) {
    this.id = num;
  }
}

function connect(part, frame) {

  part.object.geometry.vertices = [];
  var p1 = new THREE.Vector3(  data[frame][3*part.p1+0],
                               data[frame][3*part.p1+2],
                              -data[frame][3*part.p1+1] );
  var p2 = new THREE.Vector3(  data[frame][3*part.p2+0],
                               data[frame][3*part.p2+2],
                              -data[frame][3*part.p2+1] );
  if (part.p2 == -1) {
    part.object.geometry.vertices.push(p1);
    p2 = new THREE.Vector3(  data[frame][6], data[frame][6+2], -data[frame][6+1] );
    part.object.geometry.vertices.push(p2);
    p2 = new THREE.Vector3(  data[frame][24], data[frame][24+2], -data[frame][24+1] );
    part.object.geometry.vertices.push(p2);
    p2 = new THREE.Vector3( 0, 0, 0);
    part.object.geometry.vertices.push(p2);
    p2 = new THREE.Vector3(  data[frame][33], data[frame][33+2], -data[frame][33+1] );
    part.object.geometry.vertices.push(p2);
    p2 = new THREE.Vector3(  data[frame][15], data[frame][15+2], -data[frame][15+1] );
    part.object.geometry.vertices.push(p2);
    part.object.geometry.vertices.push(p1);
    
  }
  else {
    part.object.geometry.vertices.push(p1);
    part.object.geometry.vertices.push(p2);

  }
  part.object.geometry.verticesNeedUpdate = true;
}

var body_part_number = 14;
var body_part = [body_part_number];
var body_node = [body_part_number];

for (var i = 0; i < body_part_number; i++) {
  body_part[i] = Object.assign({}, bodyNodeObj );
  if (i==10 || i==13) 
    body_part[i].object = new THREE.Group();
  else {
    geometry = new THREE.Geometry();
    material = new THREE.MeshLambertMaterial( { color: body_part_color[i], transparent: true} );
    body_part[i].object = new THREE.Line( geometry, material );
  }
  body_part[i].setId(i);

  body_node[i] = Object.assign({}, bodyNodeObj );
  body_node[i].object = new THREE.Group();
  body_node[i].setId(i);
}

init();
animate();

function init() {
 //-------create body part
  var frame = data.frame;
  var point_total = data.points;
  createBodyParts();

 //-------create 3D image
  container = document.createElement( 'div' );
  document.body.appendChild( container );
  var info = document.createElement( 'div' );
  info.style.position = 'absolute';
  info.style.top = '10vh';
  info.style.width = '100%';
  info.style.textAlign = 'center';
  info.innerHTML = "Body.setPosition({1,1,1})";
  //info.innerHTML = 'Drag to spin';
  //container.appendChild( info );    //for debug

 //-------setup view point 
  camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 1, 2000 );
  var shorter = window.innerWidth;
  if (window.innerHeight < window.innerWidth)
    shorter = window.innerHeight;
  var camera_y_position = shorter*camera_dis_slope+camera_dis_shifting;
  camera.position.set( 100, camera_y_position, 0 );
  //camera.position.set( 22.3, 24.9, -35.7 );

  camera.lookAt(new THREE.Vector3(0, 0, 0));
  scene = new THREE.Scene();
  scene.background = new THREE.Color( 0x444444 );
  scene.position.set(0, 1, 0);
  scene.add( new THREE.AmbientLight( 0xe8e8e8, 1 ) );

  group = new THREE.Group();
  group.position.x = 0;
  scene.add( group );
  person = new THREE.Group();
  group.add( person );
  //group.add(nose);
  
 //------spot light
  dir_light = new THREE.DirectionalLight( 0xffffff, 1 );
  dir_light.position.set( 300, 300, -240);
  dir_light.castShadow = true;
  dir_light.shadow.mapSize.width = 1024;  // default
  dir_light.shadow.mapSize.height = 1024; // default
  dir_light.shadow.camera.left = -50;
  dir_light.shadow.camera.right = 50;
  dir_light.shadow.camera.top = 20;
  dir_light.shadow.camera.bottom = -100;
  dir_light.intensity = 1;
  scene.add( dir_light );

  //body_part[0].create(person);
  for (var i = 0; i < body_part_number; i++) {
    body_part[i].create(person);
    body_node[i].create(person);
  }

 //setup renderer, used to draw the 3D images
  renderer = new THREE.WebGLRenderer( { antialias: true } );
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.PCFSoftShadowMap;
  renderer.shadowMapSoft = false;
  container.appendChild( renderer.domElement );

  // ------test code start
  //rotateVec(1,1,1, Math.PI, 0, Math.PI, body_part[0]);
  runFrame(0);

  //-------test code end

  document.addEventListener( 'mousedown', onDocumentMouseDown, false );
  document.addEventListener( 'touchstart', onDocumentTouchStart, false );
  document.addEventListener( 'touchmove', onDocumentTouchMove, false );
  //
  window.addEventListener( 'resize', onWindowResize, false );
}

function createBodyParts() {
  var topdis = calLength(data[0][3], data[0][4], data[0][5], data[0][6], data[0][7], data[0][8]); //neck to right shoulder
  
  
  //calHeadPos(0);

  for (var i=0; i<body_part_number; i++) {
    if (i==0) {
      geometry = new THREE.SphereGeometry( lowdis, 50, 50 );
      material = new THREE.MeshLambertMaterial( { color: body_part_color[i], transparent: true} );
    }
    else if (i==4 || i==7) {
      geometry = new THREE.SphereGeometry( lowdis/2.5, 50, 50 );
      material = new THREE.MeshLambertMaterial( { color: body_part_color[i], transparent: true} );
    }
    else {
      geometry = new THREE.SphereGeometry( lowdis/3, 50, 50 );
      material = new THREE.MeshLambertMaterial( { color: 0xffffff, transparent: true} );
    }

    var tmp = new THREE.Mesh( geometry, material );
    
    body_node[i].object.add( tmp );
    body_node[i].setPosition( data[0][3*i],  -data[0][3*i+1],  data[0][3*i+2] );
  }

  for(var i=0; i<body_part_number; i++) {
    if (i==1) {
      body_part[i].top_rad = topdis;
      body_part[i].low_rad = lowdis;
    }
    else if (i==2 || i==5) {
      body_part[i].top_rad = lowdis/2;
      body_part[i].low_rad = lowdis/2;
    }
    else if (i==3 || i==6) {
      body_part[i].top_rad = lowdis/2;
      body_part[i].low_rad = lowdis*0.3;
    }
    else if (i==8 || i==11) {
      body_part[i].top_rad = lowdis/2.5;
      body_part[i].low_rad = lowdis/3;
    }
    else if (i==9 || i==12) {
      body_part[i].top_rad = lowdis/3;
      body_part[i].low_rad = lowdis/3.5;
    }
    
    if (i!=4 && i!=7 && i!=10 && i!=13) {
      body_part[i].vec = 1;
    }
  }


 //----------------------------------------------------head=0
  body_part[0].p1 = 0;
  body_part[0].p2 = 1;

 //----------------------------------------------------body=1
  body_part[1].p1 = 1;
  body_part[1].p2 = -1;

 //----------------------------------------------------rArm=2
  body_part[2].p1 = 2;
  body_part[2].p2 = 3;
 //----------------------------------------------------rForearm=3
  body_part[3].p1 = 3;
  body_part[3].p2 = 4;
 //----------------------------------------------------rHand=4
 //----------------------------------------------------lArm=5
  body_part[5].p1 = 5;
  body_part[5].p2 = 6;
 //----------------------------------------------------lForearm=6
  body_part[6].p1 = 6;
  body_part[6].p2 = 7;
 //----------------------------------------------------lHand=7
 //----------------------------------------------------rThigh=8
  body_part[8].p1 = 8;
  body_part[8].p2 = 9;
 //----------------------------------------------------rCalf=9
  body_part[9].p1 = 9;
  body_part[9].p2 = 10;
 //----------------------------------------------------lThigh=11
  body_part[11].p1 = 11;
  body_part[11].p2 = 12;
 //----------------------------------------------------lCalf=12
  body_part[12].p1 = 12;
  body_part[12].p2 = 13;


  for (var i=0; i<body_part_number; i++) {
    if (i!= 10 && i!= 13)
      connect(body_part[i], 0);
  }

 //----------------------------------------------------rFoot=10
  var tmpgroup = new THREE.Group();
  geometry = new THREE.CubeGeometry( lowdis*1.6, lowdis*0.8, lowdis*0.88);
  material = new THREE.MeshLambertMaterial( { color: body_part_color[10], transparent: true} );
  tmp = new THREE.Mesh( geometry, material );
  tmp.position.x = lowdis*0.2;
  tmp.position.y = 0;
  tmpgroup.add( tmp );
  //body_part[10].object.add( tmp );

  geometry = new THREE.CylinderGeometry( lowdis*0.8, lowdis*0.8, lowdis*0.8, 50, 20, false, 0, Math.PI/2);
  material = new THREE.MeshLambertMaterial( { color: body_part_color[10], transparent: true} );
  tmp = new THREE.Mesh( geometry, material );
  tmp.position.x = lowdis*0.7;
  tmp.rotation.x = -Math.PI/2;
  tmp.position.y = -lowdis*0.4;
  
  tmpgroup.add( tmp );
  tmpgroup.rotation.y = -Math.PI/4;
  tmpgroup.position.y = -lowdis*0.25;
  body_part[10].object.add( tmpgroup );

  
 //----------------------------------------------------lFoot=13

  var tmpgroup = new THREE.Group();
  geometry = new THREE.CubeGeometry( lowdis*1.6, lowdis*0.8, lowdis*0.8);
  material = new THREE.MeshLambertMaterial( { color: body_part_color[13], transparent: true} );

  tmp = new THREE.Mesh( geometry, material );
  tmp.position.x = lowdis*0.2;
  tmp.position.y = 0;//-8
  tmpgroup.add( tmp );


  geometry = new THREE.CylinderGeometry( lowdis*0.8, lowdis*0.8, lowdis*0.8, 50, 20, false, 0, Math.PI/2);
  material = new THREE.MeshLambertMaterial( { color: body_part_color[13], transparent: true} );
  var tmp = new THREE.Mesh( geometry, material );
  tmp.position.x = lowdis*0.7;
  tmp.rotation.x = -Math.PI/2;
  tmp.position.y = -lowdis*0.4;

  tmpgroup.add( tmp );

  tmpgroup.rotation.y = -Math.PI/8;
  tmpgroup.position.y = -lowdis*0.25;

  body_part[13].object.add( tmpgroup );
  
}

function calLength (start_x, start_y, start_z=0, end_x=0, end_y=0, end_z=0) {
  var answer;
  var x_diff = start_x - end_x;
  var y_diff = start_y - end_y;
  var z_diff = start_z - end_z;
  answer = Math.sqrt( Math.pow(x_diff, 2) + Math.pow(y_diff, 2) + Math.pow(z_diff, 2));
  return answer;
}

//---------setup the animation for spin
function onWindowResize() {
  windowHalfX = window.innerWidth / 2;
    //var zoom_ratio = angle_from_center / (angle_unit*0.5);
    //object[front_stele].scale.set( 1.8 - 0.8*zoom_ratio, 1.8 - 0.8*zoom_ratio, 1.8 - 0.8*zoom_ratio );
  windowHalfY = window.innerHeight / 2;
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize( window.innerWidth, window.innerHeight );
}
//
function onDocumentMouseDown( event ) {
  event.preventDefault();
  for(var i = 0; i < data.frame; i++ ){
    runFrame(i);
  }

  target_rotation.x = group.rotation.x;
  target_rotation.y = group.rotation.y;
  document.addEventListener( 'mousemove', onDocumentMouseMove, false );
  document.addEventListener( 'mouseup', onDocumentMouseUp, false );
  document.addEventListener( 'mouseout', onDocumentMouseOut, false );
  mouse_mouse_down.x = event.clientX - windowHalfX;
  mouse_mouse_down.y = -event.clientY + windowHalfY;
  target_rotation_on_mouse_down.x = target_rotation.x;
  target_rotation_on_mouse_down.y = target_rotation.y;

}
function onDocumentMouseMove( event ) {
  mouse_mouse_up.x = event.clientX - windowHalfX;
  mouse_mouse_up.y = -event.clientY + windowHalfY;
  target_rotation.y = target_rotation_on_mouse_down.y + ( mouse_mouse_up.x - mouse_mouse_down.x ) * 0.008;
  target_rotation.x = target_rotation_on_mouse_down.x + ( mouse_mouse_up.y - mouse_mouse_down.y ) * 0.008;
}
function onDocumentMouseUp( event ) {
  document.removeEventListener( 'mousemove', onDocumentMouseMove, false );
  document.removeEventListener( 'mouseup', onDocumentMouseUp, false );
  document.removeEventListener( 'mouseout', onDocumentMouseOut, false );
}
function onDocumentMouseOut( event ) {
  document.removeEventListener( 'mousemove', onDocumentMouseMove, false );
  document.removeEventListener( 'mouseup', onDocumentMouseUp, false );
  document.removeEventListener( 'mouseout', onDocumentMouseOut, false );
}
function onDocumentTouchStart( event ) {

    target_rotation.x = group.rotation.x;
    target_rotation.y = group.rotation.y;
  if ( event.touches.length == 1 ) {
    event.preventDefault();
    mouse_mouse_down.x = event.touches[ 0 ].pageX - windowHalfX;
    target_rotation_on_mouse_down.x = target_rotation.x;
    target_rotation_on_mouse_down.y = target_rotation.y;
  }
  else {
    target_rotation.x = group.rotation.x;    
    target_rotation.y = group.rotation.y;
  }
}

function onDocumentTouchMove( event ) {
  if ( event.touches.length == 1 ) {
    event.preventDefault();
    mouse_mouse_up.x = event.touches[ 0 ].pageX - windowHalfX;
    mouse_mouse_up.y = event.touches[ 0 ].pageY - windowHalfY;
    target_rotation.x = target_rotation_on_mouse_down.x + ( mouse_mouse_up.x - mouse_mouse_down.x ) * 0.07;
    target_rotation.y = target_rotation_on_mouse_down.y + ( mouse_mouse_up.y - mouse_mouse_down.y ) * 0.07;
  }
}

function animate() {
  requestAnimationFrame( animate );
  render();
}

function render() { //3D rotation function
  group.rotation.z += ( target_rotation.x - group.rotation.x ) * 0.008;
  group.rotation.y += ( target_rotation.y - group.rotation.y ) * 0.008;
  renderer.render( scene, camera );
}

function runFrame( num ) {
  for(var i = 0; i < body_part_number; i++) {
    if (i!= 10 && i!= 13)
      connect(body_part[i], num);

    body_node[i].setPosition( data[num][3*i],  -data[num][3*i+1],  data[num][3*i+2]);

    if ( i==0 || i==1 || i==4 || i==7 || i==10 || i==13) {
      body_part[i].setPosition( data[num][3*i],  -data[num][3*i+1],  data[num][3*i+2]);
    }
  }
  var i = 3;
  geometry = new THREE.SphereGeometry( 0.5, 50, 50 );
  material = new THREE.MeshLambertMaterial( { color: 0xffffff, transparent: true} );
  var tmp = new THREE.Mesh( geometry, material );
  tmp.position.x = 0;
  tmp.position.z = 0;
  tmp.position.y = 0;
  group.add(tmp);
}



function calHeadPos(frame) {
  var body_vec = {
    x: data[frame][3] ,
    y: data[frame][4] ,
    z: data[frame][5] 
  }

  var rhip_vec = {
    x: data[frame][24] ,
    y: data[frame][25] ,
    z: data[frame][26] 
  }

  var vec_A = Object.assign( Vector );
  vec_A.x = body_vec.y*rhip_vec.z - rhip_vec.y*body_vec.z;
  vec_A.y = body_vec.z*rhip_vec.x - rhip_vec.z*body_vec.x;
  vec_A.z = body_vec.x*rhip_vec.y - rhip_vec.x*body_vec.y;
  var vec_len = calLength(vec_A.x, vec_A.y, vec_A.z);
  vec_A.x /= vec_len;
  vec_A.y /= vec_len;
  vec_A.z /= vec_len;

  vec_A.x *= lowdis;
  vec_A.y *= lowdis;
  vec_A.z *= lowdis;

  data[frame][0] -= vec_A.x;
  data[frame][1] -= vec_A.y;
  data[frame][2] -= vec_A.z;
}

function adjustPoint () {
  for(var i=0; i<data.frame; i++) {
    origin_shift.x = (data[i][24]+data[i][33])*0.5;
    origin_shift.y = (data[i][25]+data[i][34])*0.5;
    origin_shift.z = (data[i][26]+data[i][35])*0.5;

    for(var j=0; j<data[i].length; j+=3) {
      data[i][ j ] -= origin_shift.x;
      data[i][j+1] -= origin_shift.y;
      data[i][j+2] -= origin_shift.z;
    }
  }
}



