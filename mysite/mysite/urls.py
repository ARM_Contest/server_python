"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from trips.views import home
from trips.views import upload, upload_handler
from trips.views import get_json
from trips.views import demo, show_3D
from trips.views import print_error
from trips.views import PAF, PAF_handler
from trips.views import master, slave
from trips.views import calRehab, getChooseData, getDataList

from django.views.static import serve
from mysite.settings import STATIC_ROOT

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^$', home, name="home"),

    url(r'^upload/', upload, name="upload"),
    url(r'^upload_handler/', upload_handler, name="upload_handler"),

    url(r'^get_json/', get_json, name="get_json"),

    url(r'^demo/', demo, name="demo"),
    url(r'^show_3D/', show_3D, name="Show_3D"),

    url(r'^print_error/', print_error, name="print_error"),

    url(r'^run_PAF/', PAF, name="Run_PAFs"),
    url(r'^PAF_handler/', PAF_handler, name="PAF_handler"),

    url(r'^master/', master, name="master"),
    url(r'^slave/', slave, name="slave"),

    url(r'^calRehab/', calRehab, name="calRehab"),
    url(r'^getChooseData/', getChooseData, name="getChooseData"),
    url(r'^getDataList/', getDataList, name="getDataList"),


    # url(r'^media/(?P<path>.*)$', serve,{'document_root': MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', serve,{'document_root': STATIC_ROOT}),

]
