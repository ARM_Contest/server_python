# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from datetime import datetime
from .models import Post
import shutil
import os
import json
import sys
import math
import cv2
import threading
sys.path.append("../")

from PAFs.cal_points import loadModel, runPAF
from PAFs.video_to_image import videoToImage

model = None
graph = None
#-------------------------------------------------#
#Home page

def home(request):
    return render(request, 'home.html')

#-------------------------------------------------#
#Upload & Save file test

def upload(request):
    return render(request, 'upload.html')

def upload_handler(request):
    #print(request.body)
    #print(str(request.META))
    '''
    text_file = open("output_body.txt", "a")
    text_file.write(str(request.body))
    text_file.close()
    '''
    text_file = open("output.txt", "a")
    text_file.write(str(request.FILES['file']))
    text_file.write("\n");
    text_file.close()
    handle_uploaded_file(request.FILES['file'], str(request.FILES['file']))
    return HttpResponse("Successful")

def handle_uploaded_file(file, filename):
    if not os.path.exists('upload/'):
        os.mkdir('upload/')

    with open('upload/' + filename, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)

#-------------------------------------------------#
#Return JSON to local device

def get_json(request):
    with open("./data_copy.json", 'r') as reader:
        Data = json.loads(reader.read())

    return HttpResponse(json.dumps(Data))
    #return render(request, 'Show_3D.html', {'Data': json.dumps(Data)})

#-------------------------------------------------#
#3D Rebuild

def demo(request):
    with open("./demo.json", 'r') as reader:
        Data = json.loads(reader.read())
    return render(request, 'demo.html', {'Data': json.dumps(Data)})

def show_3D(request):

    data_path = '/home/mrzhou/git/server_python/mysite/Data'

    data_list_tmp = os.listdir(data_path)
    data_list = sorted(data_list_tmp, key=str.lower, reverse=True)

    with open("./Data/"+data_list[0], 'r') as reader:
            Data = json.loads(reader.read())
    #Data = open("./data.json", "r");
    return render(request, 'show_3D.html', {'Data': json.dumps(Data), 'DataList': json.dumps(data_list)})

#-------------------------------------------------#
#print error

def print_error(request):
    if request.user.is_superuser:
        return render(request, 'print_error.html')
    else:
        return HttpResponse("Please login as super user")

#-------------------------------------------------#
#Run PAF

def PAF(request):
    return render(request, 'Run_PAF.html')

def PAF_handler(request):
    '''
    text_file = open("test.txt", "a")
    text_file.write(str(request.POST.get('input_text', False)))
    text_file.close()
    '''
    get = str(request.POST['input_text'])
    #get = str(request.POST.get('input_text', False))
    #q = QueryDict(request)
    #print(q)
    print(get)
    global model
    global graph
    if get == "1":
        model, graph = loadModel()
        print("Model Loaded")
    elif get == "2":
        runPAF(model, graph)
    else:
        v= 0
    return HttpResponse("Successful")

#-------------------------------------------------#
#Master

Slave_ready = [0, 0]
Slave_start = [-1, -1]
start_time = 0
delay_time = 5

def master(request):
    get = request.GET['move']

    global Slave_ready
    global Slave_start
    global start_time

    # check sync
    if get == "0":
        for i in range( len(Slave_ready) ):
            if Slave_ready[i] != 1:
                print(Slave_ready)
                return HttpResponse(0)
        pass
    # start recording
    elif get == "1":
        for i in range( len(Slave_start) ):
            Slave_start[i] = 1
            pass
        pass
        timestr = str(datetime.now())
        time = timestr.split(".")
        time.pop()
        # print("start")
        # print(timestr1)
        timestr1 = str(datetime.now())
        t1 = datetime.strptime(timestr1,'%Y-%m-%d %H:%M:%S.%f')
        start_time = t1.second*1000 + int(t1.microsecond/1000) + delay_time*1000
        # print(start_time)
        # time = t1.second
        shutil.rmtree('/home/mrzhou/Videos/data/')
        os.makedirs('/home/mrzhou/Videos/data/')
        return HttpResponse(time)
    elif get == "2":
        for i in range( len(Slave_start) ):
            Slave_start[i] = -1
            pass
        pass
    # resync
    elif get == "3":
        for i in range( len(Slave_ready) ):
            Slave_ready[i] = 0
            pass
        pass
    else:
        pass
    return HttpResponse(1)

#-------------------------------------------------#
#Slave

upload_finished = [0, 0]
def slave(request):
    get_move = request.GET['move']
    get_dev = int(request.GET['device'])

    global Slave_ready
    global Slave_start
    global upload_finished
    global start_time

    # start sync
    if get_move == "0":
        if Slave_ready[get_dev] == 0:
            Slave_ready[get_dev] = 1
            time = str(datetime.now())
            #print(time)
            return HttpResponse(datetime.now())
        pass
    # recording status: 1 for start, 0 for stop
    elif get_move == "1":
        result = 0
        if Slave_start[get_dev] == -1:
            result = "#"+str(-1)
            pass
        else:
            timestr = str(datetime.now())
            t1 = datetime.strptime(timestr,'%Y-%m-%d %H:%M:%S.%f')
            delay_result = start_time- (t1.second*1000 + int(t1.microsecond/1000))

            result = "#"+str(delay_result)
            pass
        return HttpResponse(result)
    elif get_move == "2":
        upload_check = 1
        upload_finished[get_dev] = 1
        for i in range(len(upload_finished)):
            if upload_finished[i] == 0:
                upload_check = 0
                break
            pass
        if upload_check:
            thread = threading.Thread(target=runCalculation(), args=())
            thread.daemon = True
            thread.start()

            upload_finished = [0, 0]
            pass
        print(upload_finished)
        return HttpResponse(1)
    else:
        pass
    return HttpResponse(0)

def runCalculation():

    video_path = '/home/mrzhou/Videos/data/'

    video_list = os.listdir(video_path)
    video_list.sort()

    print("video to image start")
    videoToImage(video_path + video_list[0], video_path + video_list[1])
    print("video to image success")

    print("Model Loading")
    model, graph = loadModel()
    print("Model Loaded")

    print("PAF start")
    thread = threading.Thread(target=runPAF(model, graph), args=())
    thread.daemon = True
    thread.start()
    #runPAF(model, graph)
    print("PAF succeed")
    # upload_finished = [0, 0]
    pass

#-------------------------------------------------#
# return data list
def getDataList(request):
    #print(request)
    #print(request.POST)

    data_path = '/home/mrzhou/git/server_python/mysite/Data'

    data_list_tmp = os.listdir(data_path)
    data_list = sorted(data_list_tmp, key=str.lower, reverse=True)

    print(data_list)
    return HttpResponse(json.dumps(data_list), content_type="application/json")

#-------------------------------------------------#
# return other data
def getChooseData(request):
    #print(request)
    #print(request.POST)

    data_name = request.GET['data_name']
    with open("/home/mrzhou/git/server_python/mysite/Data/"+data_name, 'r') as reader:
            Data = json.loads(reader.read())

    print(Data)
    return HttpResponse(json.dumps(Data), content_type="application/json")



#-------------------------------------------------#
# calculate rehabiliation

def calRehab(request):
    print(request)
    print(request.POST)

    data_name = request.POST['data_name']
    choice = request.POST['choice']
    adjust_data = None
    with open("/home/mrzhou/git/server_python/mysite/Data/"+data_name+".json", 'r') as reader:
            Data = json.loads(reader.read())
    if choice == "0":
        adjust_data = rightElbow(Data)
    elif choice == "1":
        pass
    elif choice == "2":
        pass
    elif choice == "3":
        pass
    else:
        pass

    #print(adjust_data)
    return HttpResponse(json.dumps(adjust_data), content_type="application/json")
    #return JsonResponse(adjust_data)

def rightElbow(data):
    adjust_data = {}

    adjust_data['adjust_node'] = [2, 3, 4]
    adjust_data['adjust_part'] = [ [2, 2, 3], [3, 3, 4] ] #[connect_part, node1, node2]

    adjust_data['accurate'] = []
    adjust_data['count'] = []

    pose_reset = True
    correct_move=0
    for frame_num in range(data['frame']):
        #count
        #v1 => arm, v2 => forarm
        frame = str(frame_num)
        #print(data)
        v1_x = data[frame][6] - data[frame][9]
        v1_y = data[frame][7] - data[frame][10]
        v1_z = data[frame][8] - data[frame][11]

        v2_x = data[frame][12] - data[frame][9]
        v2_y = data[frame][13] - data[frame][10]
        v2_z = data[frame][14] - data[frame][11]
        result = calAngle(v1_x, v1_y, v1_z, v2_x, v2_y, v2_z)

        body_x = data[frame][3] - data[frame][6]
        body_y = data[frame][4] - data[frame][7]
        body_z = data[frame][5] - data[frame][8]

        #accurate hand
        hand_angle = calAngle(v1_x, v1_y, v1_z, body_x, body_y, body_z)
        if hand_angle[0]>1.570796 :
            hand_angle[0] = math.pi - hand_angle[0]

        hand_acc = hand_angle[0]*2/math.pi

        #accurate elbow
        elbow_angle = calAngle(v2_x, v2_y, v2_z, body_x, body_y, body_z)
        if elbow_angle[0]>1.570796 :
            elbow_angle[0] = math.pi - elbow_angle[0]

        elbow_acc = elbow_angle[0]*2/math.pi
        acc = ( hand_acc + elbow_acc ) * 50

        adjust_data['accurate'].append( round(acc, 2) )


        if pose_reset:
            if result[0]<0.82 :
                print(result[0])
                correct_move+=1
                pose_reset = None
        else :
            if result[0]>2.9 :
                pose_reset = True;

        adjust_data['count'].append(correct_move)


        # Show Ideal position
        arm_len = calLength(v1_x, v1_y, v1_z)
        forearm_len = calLength(v2_x, v2_y, v2_z)
        v1_x = -v1_x
        v1_y = -v1_y
        v1_z = -v1_z

        #for arm
        vec_body_arm = crossProduct(v1_x, v1_y, v1_z, body_x, body_y, body_z)
        vec_bodyarm_body = crossProduct(vec_body_arm[0], vec_body_arm[1], vec_body_arm[2], body_x, body_y, body_z)
        check_angle = calAngle(vec_bodyarm_body[0], vec_bodyarm_body[1],vec_bodyarm_body[2], v1_x, v1_y, v1_z)
        if check_angle[0] > math.pi/2 :
            vec_bodyarm_body[0] *= -1
            vec_bodyarm_body[1] *= -1
            vec_bodyarm_body[2] *= -1

        tmplength = calLength(vec_bodyarm_body[0], vec_bodyarm_body[1], vec_bodyarm_body[2])
        param = arm_len/tmplength
        vec_bodyarm_body[0] *= param
        vec_bodyarm_body[1] *= param
        vec_bodyarm_body[2] *= param
        correct_elbow_pos_x = data[frame][6] + vec_bodyarm_body[0]
        correct_elbow_pos_y = data[frame][7] + vec_bodyarm_body[1]
        correct_elbow_pos_z = data[frame][8] + vec_bodyarm_body[2]

        # for forearm
        vec_body_hand = crossProduct(v2_x, v2_y, v2_z, body_x, body_y, body_z)
        vec_bodyhand_body = crossProduct(vec_body_hand[0], vec_body_hand[1], vec_body_hand[2], body_x, body_y, body_z)
        check_angle = calAngle(vec_bodyhand_body[0], vec_bodyhand_body[1],vec_bodyhand_body[2], v2_x, v2_y, v2_z)
        if check_angle[0] > math.pi/2 :
            vec_bodyhand_body[0] *= -1
            vec_bodyhand_body[1] *= -1
            vec_bodyhand_body[2] *= -1

        tmplength = calLength(vec_bodyhand_body[0], vec_bodyhand_body[1], vec_bodyhand_body[2])
        param = forearm_len/tmplength
        vec_bodyhand_body[0] *= param
        vec_bodyhand_body[1] *= param
        vec_bodyhand_body[2] *= param

        correct_hand_pos_x = correct_elbow_pos_x + vec_bodyhand_body[0]
        correct_hand_pos_y = correct_elbow_pos_y + vec_bodyhand_body[1]
        correct_hand_pos_z = correct_elbow_pos_z + vec_bodyhand_body[2]

        adjust_data[str(frame)] = [ round(data[frame][6], 4), round(data[frame][7], 4), round(data[frame][8], 4), round(correct_elbow_pos_x, 4), round(correct_elbow_pos_y, 4), round(correct_elbow_pos_z, 4), round(correct_hand_pos_x, 4), round(correct_hand_pos_y, 4), round(correct_hand_pos_z, 4) ]
    return adjust_data

def calLength(p1_x, p1_y, p1_z, p2_x=0, p2_y=0, p2_z=0):
    x_diff = p1_x - p2_x
    y_diff = p1_y - p2_y
    z_diff = p1_z - p2_z
    answer = math.sqrt( math.pow(x_diff, 2) + math.pow(y_diff, 2) + math.pow(z_diff, 2))
    return answer

def calAngle(v1_x, v1_y, v1_z=0, v2_x=0, v2_y=0, v2_z=0):
    answer = [0,0,0]
    dot_product = v1_x*v2_x + v1_y*v2_y + v1_z*v2_z
    v1_len = calLength(v1_x, v1_y, v1_z)
    v2_len = calLength(v2_x, v2_y, v2_z)
    cosine_theta = dot_product/v1_len/v2_len
    theta = math.acos(cosine_theta)
    answer[0] = theta
    answer[1] = v1_len
    answer[2] = v2_len
    return answer

def crossProduct(v1_x, v1_y, v1_z, v2_x, v2_y, v2_z):
    answer = [0,0,0]
    answer[0] = v1_y*v2_z - v1_z*v2_y
    answer[1] = v1_z*v2_x - v1_x*v2_z
    answer[2] = v1_x*v2_y - v1_y*v2_x
    return answer


if __name__ == '__main__':
    with open("../data.json", 'r') as reader:
            Data = json.loads(reader.read())
    adjust_data = rightElbow(Data)
    print(adjust_data)
