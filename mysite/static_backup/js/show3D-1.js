
//--------variable for basic scene
var container;
var camera, scene, renderer;
var group, person;
var dir_light;
var target_rotation = {x: 0, y: 0};
var target_rotation_on_mouse_down = {x: 0, y: 0};

var camera_dis_slope = -0.02247;
var camera_dis_shifting = 49;

//--------Variable for controll
var mouse_mouse_up = { x: 0, y: 0};
var mouse_mouse_down = { x: 0, y: 0};
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

//---------read body part position
var data = {
  "frame": 2,
  "points": 14,
  "1" : [
    0,   0, 65,
    0,   0, 50,
    0,  20, 50,  0,  26,  20,  0,  25,   0, 
    0, -20, 50,  0, -26,  20,  0, -25,   0,  
    0,  15,  0,  0,  20, -44,  0,  25, -84,
    0, -15,  0,  0, -20, -44,  0, -25, -84
  ],
  "2" : [
    0,   0, 65,
    0,   0, 50,
    0,  20, 50,  10,  26,  20, 20,  25,   5,
    0, -20, 50,  10, -26,  20, 20, -25,   5,  
    0,  15,  0,  30,  20, -30,  10,  25, -64,
    0, -15,  0,   5, -20, -44,  10, -25, -84
  ]
}
//var requestURL = './data.json';
//var file = './data.json';
//var data = document.open(file);
//var contents = data.document.body.innerText; 
/*
var data = $.getJSON('./data.json', function () {
    console.log("success");
})

var request = new XMLHttpRequest();
request.open('GET', requestURL);
request.responseType = 'json';
request.send();
var data = request.response;
*/


//----------Setup each body part color 
var body_part_color = [ 0x28ff28, 0x00ffff,           //hand, body
                        0xb766ad, 0xff0000, 0xffd306, //Right hand
                        0xb766ad, 0xff0000, 0xffd306, //Left hand
                        0xb766ad, 0xff0000, 0xffd306, //Right leg
                        0xb766ad, 0xff0000, 0xffd306 ]//Left leg

/*
  number  =>  Body Part
      0   =>  Head
      1   =>  Body

      2   =>  Right Arm
      3   =>  Right Forearm
      4   =>  Right hand

      5   =>  Left Arm
      6   =>  Left Forearm
      7   =>  Left hand

      8   =>  Right Thigh
      9   =>  Right Calf
     10   =>  Right Foot

     11   =>  Left Thigh
     12   =>  Left Calf
     13   =>  Left Foot
*/

var bodyPartObj = {
  length: 0,
  id: 0,
  object: null,
  vec_x: 0,
  vec_y: 0, 
  vec_z: 0,

  create: function( input_group ){
    input_group.add( this.object );
  },
  setPosition: function( start_x, start_y, start_z, end_x=0, end_y=0, end_z=0 ) {       //use unit vector
    var vector = { x: (end_x-start_x), y: (end_y-start_y), z: (end_z-start_z)};
    //normalize
    /*
    var vec_len = calLength(vector.x, vector.y, vector.z);
    vector.x /= vec_len;
    vector.y /= vec_len;
    vector.z /= vec_len;
    */

    this.object.position.x = start_x;
    
    if (this.id >= 11) 
      this.object.position.z = start_y+7;
    else if (this.id >= 8) 
      this.object.position.z = start_y-7;
    else
      this.object.position.z = start_y;

    this.object.position.y = start_z;

    // x axis, x = 0
    var x_rot_angle = 0; // angle needs to rotate along x axis 
    x_rot_angle = calVecAngle(this.vec_y, this.vec_z, vector.y, vector.z);
    this.object.rotation.x = x_rot_angle;

    // y axis, y = 0
    var y_rot_angle = 0; // angle needs to rotate along x axis 
    y_rot_angle = calVecAngle(this.vec_x, this.vec_z, vector.x, vector.z);
    this.object.rotation.z = -y_rot_angle;


    // z axis, z = 0   in three.js, z point to right, y point to top
    var z_rot_angle = 0; // angle needs to rotate along z axis
    z_rot_angle = calVecAngle(this.vec_x, this.vec_y, vector.x, vector.y);
    this.object.rotation.y = z_rot_angle;

  },
  setId: function( num ) {
    this.id = num;
  }
}

var body_part_number = 14;
var body_part = [body_part_number];

for (var i = 0; i < body_part_number; i++) {
  body_part[i] = Object.assign({}, bodyPartObj );
  body_part[i].object = new THREE.Group();
  body_part[i].setId(i);
}

init();
animate();

function init() {
  //var test = calLength(1,2,3);

 //-------create body part
  var frame = data.frame;
  var point_total = data.points;
  createBodyParts();

 //-------create 3D image
  container = document.createElement( 'div' );
  document.body.appendChild( container );
  var info = document.createElement( 'div' );
  info.style.position = 'absolute';
  info.style.top = '10vh';
  info.style.width = '100%';
  info.style.textAlign = 'center';
  info.innerHTML = "Body.setPosition({1,1,1})";
  //info.innerHTML = 'Drag to spin';
  //container.appendChild( info );    //for debug

 //-------setup view point 
  camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 1, 2000 );
  var shorter = window.innerWidth;
  if (window.innerHeight < window.innerWidth)
    shorter = window.innerHeight;
  var camera_y_position = shorter*camera_dis_slope+camera_dis_shifting;
  camera.position.set( 400, camera_y_position, 0 );

  camera.lookAt(new THREE.Vector3(0, 0, 0));
  scene = new THREE.Scene();
  scene.background = new THREE.Color( 0x444444 );
  scene.position.set(0, 1, 0);
  scene.add( new THREE.AmbientLight( 0xe8e8e8, 1 ) );

  group = new THREE.Group();
  group.position.x = 100;
  scene.add( group );
  person = new THREE.Group();
  group.add( person );
  
 //------spot light
  dir_light = new THREE.DirectionalLight( 0xffffff, 1 );
  dir_light.position.set( 300, 300, -240);
  dir_light.castShadow = true;
  dir_light.shadow.mapSize.width = 1024;  // default
  dir_light.shadow.mapSize.height = 1024; // default
  dir_light.shadow.camera.left = -50;
  dir_light.shadow.camera.right = 50;
  dir_light.shadow.camera.top = 20;
  dir_light.shadow.camera.bottom = -100;
  dir_light.intensity = 1;
  scene.add( dir_light );

  //body_part[0].create(person);
  for (var i = 0; i < body_part_number; i++) {
    body_part[i].create(person);
  }

 //setup renderer, used to draw the 3D images
  renderer = new THREE.WebGLRenderer( { antialias: true } );
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.PCFSoftShadowMap;
  renderer.shadowMapSoft = false;
  container.appendChild( renderer.domElement );

  // ------test code start
  runFrame(1);

  //-------test code end

  document.addEventListener( 'mousedown', onDocumentMouseDown, false );
  document.addEventListener( 'touchstart', onDocumentTouchStart, false );
  document.addEventListener( 'touchmove', onDocumentTouchMove, false );
  //
  window.addEventListener( 'resize', onWindowResize, false );
}

function calVecAngle( start_1, start_2, end_1, end_2 ) {
  //Math.pow(x_diff, 2) + Math.pow(y_diff, 2) + Math.pow(z_diff, 2);
  var answer=0;
  var dot_product = start_1*end_1 + start_2*end_2;
  var cross_product = start_1*end_2 - start_2*end_1;
  var a_len_pow2 = Math.pow(start_1, 2) + Math.pow(start_2, 2);
  var b_len_pow2 = Math.pow(end_1, 2) + Math.pow(end_2, 2);

  var cos_theta = Math.sqrt(dot_product*dot_product/a_len_pow2/b_len_pow2);

  var abs_cos = Math.abs(cos_theta) ;
  if(cos_theta == 0)
    abs_cos = 0;
  answer = Math.abs(Math.acos( abs_cos ));
  if (cross_product<0) {
    answer = answer*(-1);
  }
  if (cos_theta == -1) 
    answer = -Math.PI;
  if(a_len_pow2 ==0 || b_len_pow2 == 0)
    answer = 0;
  if (answer == 0) 
    return 0;
  return -answer;
}

function createBodyParts() {
  var geometry = null;
  var material = null;

 //----------------------------------------------------Head=0
  geometry = new THREE.SphereGeometry( 10, 50, 50 );
  material = new THREE.MeshLambertMaterial( { color: body_part_color[0], transparent: true} );
  body_part[0].object.add( new THREE.Mesh( geometry, material ) );

 //----------------------------------------------------body=1
  //calculate body perameter
  var topdis = calLength(data[1][3], data[1][4], data[1][5], data[1][6], data[1][7], data[1][8]); //neck to right shoulder
  var lowdis = calLength(data[1][24], data[1][25], data[1][26], data[1][33], data[1][34], data[1][35])/2; //right hip to left hip
  var height = calLength(data[1][24], data[1][25], data[1][26], data[1][3], data[1][4], data[1][5]); //neck to right shoulder
  var tmpgroup = new THREE.Group();
  geometry = new THREE.CylinderGeometry( topdis, lowdis, height, 5, 20, false, 0, Math.PI-0.3 );
  material = new THREE.MeshLambertMaterial( { color: body_part_color[1], transparent: true} );
  material.side = THREE.DoubleSide;
  var tmp = new THREE.Mesh( geometry, material );
  tmpgroup.add( tmp );
  geometry = new THREE.CubeGeometry( lowdis, height, 0.3 );
  material = new THREE.MeshLambertMaterial( { color: body_part_color[1], transparent: true} );
  tmp = new THREE.Mesh( geometry, material );
  tmp.rotation.y = -Math.PI*0.5;
  tmp.position.x = 0;
  tmp.position.z = lowdis/2;
  tmpgroup.add( tmp );

  geometry = new THREE.CubeGeometry( lowdis, height, 0.3 );
  material = new THREE.MeshLambertMaterial( { color: body_part_color[1], transparent: true} );
  tmp = new THREE.Mesh( geometry, material );
  tmp.rotation.y = Math.PI*0.5-0.3;
  tmp.position.x = lowdis*(0.14776);
  tmp.position.z = -lowdis*0.477668;
  tmpgroup.add( tmp );

  tmpgroup.rotation.y = 0.15;
  tmpgroup.position.x = -lowdis/2-2.5;
  tmpgroup.position.y = -0.5*height;
  body_part[1].object.add( tmpgroup );
  body_part[1].length = height;

  body_part[1].vec_z = -1;

 //----------------------------------------------------rArm=2
  height = calLength(data[1][6], data[1][7], data[1][8], data[1][9], data[1][10], data[1][11]);
  geometry = new THREE.CylinderGeometry( 5, 5, height, 50, 20 );
  material = new THREE.MeshLambertMaterial( { color: body_part_color[2], transparent: true} );
  tmp = new THREE.Mesh( geometry, material );
  tmp.position.z = 0;
  tmp.position.y = -height/2;
  body_part[2].object.add( tmp );
  body_part[2].length = height;
  body_part[2].vec_z = -1;

 //----------------------------------------------------rForearm=3
  height = calLength(data[1][12], data[1][13], data[1][14], data[1][9], data[1][10], data[1][11]);
  geometry = new THREE.CylinderGeometry( 5, 3, height, 50, 20 );
  material = new THREE.MeshLambertMaterial( { color: body_part_color[3], transparent: true} );
  tmp = new THREE.Mesh( geometry, material );
  tmp.position.z = 0;
  tmp.position.y = -height/2;
  body_part[3].object.add( tmp );
  body_part[3].length = height;
  body_part[3].vec_z = -1;

 //----------------------------------------------------rHand=4
  geometry = new THREE.SphereGeometry( 4, 50, 50 );
  material = new THREE.MeshLambertMaterial( { color: body_part_color[4], transparent: true} );
  tmp = new THREE.Mesh( geometry, material );
  tmp.position.z = 0;
  tmp.position.y = -4;
  body_part[4].object.add( tmp );

 //----------------------------------------------------lArm=5
  height = calLength(data[1][15], data[1][16], data[1][17], data[1][18], data[1][19], data[1][20]);
  geometry = new THREE.CylinderGeometry( 5, 5, height, 50, 20 );
  material = new THREE.MeshLambertMaterial( { color: body_part_color[5], transparent: true} );
  tmp = new THREE.Mesh( geometry, material );
  tmp.position.z = 0;
  tmp.position.y = -height/2;
  body_part[5].object.add( tmp );
  body_part[5].length = height;
  body_part[5].vec_z = -1;

 //----------------------------------------------------lForearm=6
  height = calLength(data[1][21], data[1][22], data[1][23], data[1][18], data[1][19], data[1][20]);
  geometry = new THREE.CylinderGeometry( 5, 3, height, 50, 20 );
  material = new THREE.MeshLambertMaterial( { color: body_part_color[3], transparent: true} );
  tmp = new THREE.Mesh( geometry, material );
  tmp.position.z = 0;
  tmp.position.y = -height/2;
  body_part[6].object.add( tmp );
  body_part[6].length = height;
  body_part[6].vec_z = -1;

 //----------------------------------------------------lHand=7
  geometry = new THREE.SphereGeometry( 4, 50, 50 );
  material = new THREE.MeshLambertMaterial( { color: body_part_color[7], transparent: true} );tmp = new THREE.Mesh( geometry, material );
  tmp.position.z = 0;
  tmp.position.y = -4;
  body_part[7].object.add( tmp );

 //----------------------------------------------------rThigh=8
  height = calLength(data[1][24], data[1][25], data[1][26], data[1][27], data[1][28], data[1][29]);
  geometry = new THREE.CylinderGeometry( lowdis/2.5, lowdis/3, height, 50, 20 );
  material = new THREE.MeshLambertMaterial( { color: body_part_color[8], transparent: true} );
  tmp = new THREE.Mesh( geometry, material );
  tmp.position.z = 0;
  tmp.position.x = 0;
  tmp.position.y = -height/2;
  body_part[8].object.add( tmp );
  body_part[8].length = height;
  body_part[8].vec_z = -1;

 //----------------------------------------------------rCalf=9
  height = calLength(data[1][30], data[1][31], data[1][32], data[1][27], data[1][28], data[1][29]);
  geometry = new THREE.CylinderGeometry( lowdis/3, 4, height, 50, 20 );
  material = new THREE.MeshLambertMaterial( { color: body_part_color[9], transparent: true} );
  tmp = new THREE.Mesh( geometry, material );
  tmp.position.z = 0;
  tmp.position.x = 0;
  tmp.position.y = -height/2;  
  //tmp.rotation.z = Math.PI;
  body_part[9].object.add( tmp );
  body_part[9].length = height;
  body_part[9].vec_z = -1;

 //----------------------------------------------------rFoot=10
  geometry = new THREE.CubeGeometry( 16, 8, 8);
  material = new THREE.MeshLambertMaterial( { color: body_part_color[10], transparent: true} );
  tmp = new THREE.Mesh( geometry, material );
  tmp.position.x = 2;
  tmp.position.y = 0;

  body_part[10].object.add( tmp );

  geometry = new THREE.CylinderGeometry( 8, 8, 8, 50, 20, false, 0, Math.PI/2);
  material = new THREE.MeshLambertMaterial( { color: body_part_color[10], transparent: true} );
  tmp = new THREE.Mesh( geometry, material );
  tmp.position.x = 7;
  tmp.rotation.x = -Math.PI/2;
  tmp.position.y = -4;

  body_part[10].object.add( tmp );

 //----------------------------------------------------lThigh=11
  height = calLength(data[1][33], data[1][34], data[1][35], data[1][36], data[1][37], data[1][38]);
  geometry = new THREE.CylinderGeometry( lowdis/2.5, lowdis/3, height, 50, 20 );
  material = new THREE.MeshLambertMaterial( { color: body_part_color[11], transparent: true} );

  tmp = new THREE.Mesh( geometry, material );
  tmp.position.z = 0;
  tmp.position.x = 0;
  tmp.position.y = -height/2;
  body_part[11].object.add( tmp );
  body_part[11].length = height;
  body_part[11].vec_z = -1;

 //----------------------------------------------------lCalf=12
  height = calLength(data[1][39], data[1][40], data[1][41], data[1][36], data[1][37], data[1][38]);
  geometry = new THREE.CylinderGeometry( lowdis/3, 4, height, 50, 20 );
  material = new THREE.MeshLambertMaterial( { color: body_part_color[12], transparent: true} );
  tmp = new THREE.Mesh( geometry, material );
  tmp.position.z = 0;
  tmp.position.x = 0;
  tmp.position.y = -height/2;
  body_part[12].object.add( tmp );
  body_part[12].length = height;
  body_part[12].vec_z = -1;

 //----------------------------------------------------lFoot=13
  geometry = new THREE.CubeGeometry( 16, 8, 8);
  material = new THREE.MeshLambertMaterial( { color: body_part_color[13], transparent: true} );

  tmp = new THREE.Mesh( geometry, material );
  tmp.position.x = 2;
  tmp.position.y = 0;//-8

  body_part[13].object.add( tmp );

  geometry = new THREE.CylinderGeometry( 8, 8, 8, 50, 20, false, 0, Math.PI/2);
  material = new THREE.MeshLambertMaterial( { color: body_part_color[13], transparent: true} );
  var tmp = new THREE.Mesh( geometry, material );
  tmp.position.x = 7;
  tmp.rotation.x = -Math.PI/2;
  tmp.position.y = -4;

  body_part[13].object.add( tmp );
  
}

function calLength (start_x, start_y, start_z=0, end_x=0, end_y=0, end_z=0) {
  var answer;
  var x_diff = start_x - end_x;
  var y_diff = start_y - end_y;
  var z_diff = start_z - end_z;
  answer = Math.sqrt( Math.pow(x_diff, 2) + Math.pow(y_diff, 2) + Math.pow(z_diff, 2));
  return answer;
}

//---------setup the animation for spin
function onWindowResize() {
  windowHalfX = window.innerWidth / 2;
    var zoom_ratio = angle_from_center / (angle_unit*0.5);
    //object[front_stele].scale.set( 1.8 - 0.8*zoom_ratio, 1.8 - 0.8*zoom_ratio, 1.8 - 0.8*zoom_ratio );
  windowHalfY = window.innerHeight / 2;
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize( window.innerWidth, window.innerHeight );
}
//
function onDocumentMouseDown( event ) {
  event.preventDefault();
  for(var i = 1; i <= data.frame; i++ ){
    runFrame(i);
  }

  target_rotation.x = group.rotation.x;
  target_rotation.y = group.rotation.y;
  document.addEventListener( 'mousemove', onDocumentMouseMove, false );
  document.addEventListener( 'mouseup', onDocumentMouseUp, false );
  document.addEventListener( 'mouseout', onDocumentMouseOut, false );
  mouse_mouse_down.x = event.clientX - windowHalfX;
  mouse_mouse_down.y = -event.clientY + windowHalfY;
  target_rotation_on_mouse_down.x = target_rotation.x;
  target_rotation_on_mouse_down.y = target_rotation.y;

}
function onDocumentMouseMove( event ) {
  mouse_mouse_up.x = event.clientX - windowHalfX;
  mouse_mouse_up.y = -event.clientY + windowHalfY;
  target_rotation.y = target_rotation_on_mouse_down.y + ( mouse_mouse_up.x - mouse_mouse_down.x ) * 0.008;
  target_rotation.x = target_rotation_on_mouse_down.x + ( mouse_mouse_up.y - mouse_mouse_down.y ) * 0.008;
}
function onDocumentMouseUp( event ) {
  document.removeEventListener( 'mousemove', onDocumentMouseMove, false );
  document.removeEventListener( 'mouseup', onDocumentMouseUp, false );
  document.removeEventListener( 'mouseout', onDocumentMouseOut, false );
}
function onDocumentMouseOut( event ) {
  document.removeEventListener( 'mousemove', onDocumentMouseMove, false );
  document.removeEventListener( 'mouseup', onDocumentMouseUp, false );
  document.removeEventListener( 'mouseout', onDocumentMouseOut, false );
}
function onDocumentTouchStart( event ) {

    target_rotation.x = group.rotation.x;
    target_rotation.y = group.rotation.y;
  if ( event.touches.length == 1 ) {
    event.preventDefault();
    mouse_mouse_down.x = event.touches[ 0 ].pageX - windowHalfX;
    target_rotation_on_mouse_down.x = target_rotation.x;
    target_rotation_on_mouse_down.y = target_rotation.y;
  }
  else {
    target_rotation.x = group.rotation.x;    
    target_rotation.y = group.rotation.y;
  }
}

function onDocumentTouchMove( event ) {
  if ( event.touches.length == 1 ) {
    event.preventDefault();
    mouse_mouse_up.x = event.touches[ 0 ].pageX - windowHalfX;
    mouse_mouse_up.y = event.touches[ 0 ].pageY - windowHalfY;
    target_rotation.x = target_rotation_on_mouse_down.x + ( mouse_mouse_up.x - mouse_mouse_down.x ) * 0.07;
    target_rotation.y = target_rotation_on_mouse_down.y + ( mouse_mouse_up.y - mouse_mouse_down.y ) * 0.07;
  }
}

function animate() {
  requestAnimationFrame( animate );
  render();
}

function render() { //3D rotation function
  group.rotation.x += ( target_rotation.x - group.rotation.x ) * 0.008;
  group.rotation.y += ( target_rotation.y - group.rotation.y ) * 0.008;
  renderer.render( scene, camera );
}

function runFrame( num ) {
  for(var i = 0; i < body_part_number; i++) {
    if ( i==0 || i==1 || i==4 || i==7 || i==10 || i==13) {
      body_part[i].setPosition( data[num][3*i],  data[num][3*i+1],  data[num][3*i+2] );
    }
    else {
      body_part[i].setPosition( data[num][3*i],  data[num][3*i+1],  data[num][3*i+2], data[num][3*i+3],  data[num][3*i+4], data[num][3*i+5] );
    }
  }
}

/*
var Head = Object.assign({}, bodyPart );

var Body = Object.assign({}, bodyPart );

var rArm = Object.assign({}, bodyPart );

var rForearm = Object.assign({}, bodyPart );

var rHand = Object.assign({}, bodyPart );

var lArm = Object.assign({}, bodyPart );

var lForearm = Object.assign({}, bodyPart );

var lHand = Object.assign({}, bodyPart );

var rThigh = Object.assign({}, bodyPart );

var rCalf = Object.assign({}, bodyPart );

var rFoot = Object.assign({}, bodyPart );

var lThigh = Object.assign({}, bodyPart );

var lCalf = Object.assign({}, bodyPart );

var lFoot = Object.assign({}, bodyPart );
*/

/*

var lCalf = {
  init: function() {

  },
  setPosition: function( startx, starty, startz, endx=0, endy=0, endz=0 ) {       //set the starting point
    
  }
};
*/
/*
  body_part[0].setPosition( data[2][0],  data[2][1],  data[2][2] );
  body_part[1].setPosition( data[2][3],  data[2][4],  data[2][5] );

  body_part[2].setPosition( data[2][6],  data[2][7],  data[2][8],  
                            data[2][9],  data[2][10], data[2][11] );
  body_part[3].setPosition( data[2][9],  data[2][10], data[2][11], 
                            data[2][12], data[2][13], data[2][14] );
  body_part[4].setPosition( data[2][12], data[2][13], data[2][14] );

  body_part[5].setPosition( data[2][15], data[2][16], data[2][17],
                            data[2][18], data[2][19], data[2][20] );
  body_part[6].setPosition( data[2][18], data[2][19], data[2][20],  
                            data[2][21], data[2][22], data[2][23] );
  body_part[7].setPosition( data[2][21], data[2][22], data[2][23] );

  body_part[8].setPosition(  data[2][24], data[2][25], data[2][26],  
                             data[2][27], data[2][28], data[2][29] );
  body_part[9].setPosition(  data[2][27], data[2][28], data[2][29],  
                             data[2][30], data[2][31], data[2][32] );
  body_part[10].setPosition( data[2][30], data[2][31], data[2][32] );

  body_part[11].setPosition( data[2][33], data[2][34], data[2][35],  
                             data[2][36], data[2][37], data[2][38] );
  body_part[12].setPosition( data[2][36], data[2][37], data[2][38],  
                             data[2][39], data[2][40], data[2][41] );
  body_part[13].setPosition( data[2][39], data[2][40], data[2][41] );
  */

