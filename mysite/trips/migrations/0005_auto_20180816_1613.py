# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trips', '0004_auto_20180808_1108'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='photo',
            field=models.URLField(blank=True),
        ),
    ]
