import json
import sys
import csv

data_in = open("test.json")
json_value = data_in.read()
data=json.loads(json_value)

output = open("output.csv", "w")

csvwriter = csv.writer(output)

data_size = data["frame"]-1
for i in range(0,data_size):
    csvwriter.writerow(data[str(i)])

