//--------variable for basic scene
var container;
var camera, scene, renderer;
var group, person;
var dir_light;
var target_rotation = {x: 0, y: 0};
var target_rotation_on_mouse_down = {x: 0, y: 0};

var camera_y_position=0;
var camera_dis = 100;

//--------Variable for controll
var mouse_mouse_up = { x: 0, y: 0 };
var mouse_mouse_down = { x: 0, y: 0 };
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

var geometry = null;
var material = null;

var origin_shift = { x: 0,  y: 0,  z: 0 }

var lowdis=0;

var time_delay=200;

/*------------nose point
geometry = new THREE.SphereGeometry( 0.5, 50, 50 );
material = new THREE.MeshLambertMaterial( { color: 0xffffff, transparent: true} );
var nose = new THREE.Mesh( geometry, material );
nose.position.x = data[0][0];
nose.position.z = -data[0][1];
nose.position.y = data[0][2];
group.add(nose);
*/

/*------------origin point
geometry = new THREE.SphereGeometry( 0.5, 50, 50 );
material = new THREE.MeshLambertMaterial( { color: 0xffffff, transparent: true} );
var tmp = new THREE.Mesh( geometry, material );
tmp.position.x = 0;
tmp.position.z = 0;
tmp.position.y = 0;
group.add(tmp);
*/

lowdis = data["len_pre"];//right hip to left hip
camera_y_position = lowdis;
//----------Setup each body part color 
var body_part_color = [ 0x28ff28, 0x00ffff,           //hand, body
                        0xb766ad, 0xff0000, 0xffd306, //Right hand
                        0xb766ad, 0xff0000, 0xffd306, //Left hand
                        0xb766ad, 0xff0000, 0xffd306, //Right leg
                        0xb766ad, 0xff0000, 0xffd306 ]//Left leg

/*
  number  =>  Body Part         =>  node 

      0   =>  Neck              =>  Nose
      1   =>  Body              =>  Neck

      2   =>  Right Arm         =>  Right Shoulder 
      3   =>  Right Forearm     =>  Right Elbow
      4   =>  Right hand        =>  Right Wrist

      5   =>  Left Arm          =>  Left Shoulder 
      6   =>  Left Forearm      =>  Left Elbow
      7   =>  Left hand         =>  Left Wrist

      8   =>  Right Thigh       =>  Right Hip
      9   =>  Right Calf        =>  Right Knee
     10   =>  Right Foot        =>  Right Ankle

     11   =>  Left Thigh        =>  Left Hip
     12   =>  Left Calf         =>  Left Knee
     13   =>  Left Foot         =>  Left Ankle
*/

var bodyNodeObj = {
  id: 0,
  enable: 1,
  object: null,
  //top_rad: 0,
  //low_rad: 0,
  vec: 0,             // if the body part has derection, set to 1
  p1: 0,              // the point the body part connect
  p2: 0,

  setPosition: function(start_x, start_y, start_z) {       //use unit vector
    if (  this.vec == 0 ) {
      this.object.position.x = start_x;
      this.object.position.z = -start_y;
      this.object.position.y = start_z;
    }
  },
  setId: function( num ) {
    this.id = num;
  }
}

var body_part_number = data["points"];
var body_part = [body_part_number];
var body_node = [body_part_number];
var body_part_ideal = [body_part_number];
var body_node_ideal = [body_part_number];
var frame_num = data["frame"];
var play_animation = [frame_num];

for (var i = 0; i < body_part_number; i++) {
  body_part[i] = Object.assign({}, bodyNodeObj );
  body_part_ideal[i] = Object.assign({}, bodyNodeObj );
  
  geometry = new THREE.Geometry();
  material = new THREE.MeshLambertMaterial( { color: body_part_color[i], transparent: true} );
  body_part[i].object = new THREE.Line( geometry, material );
  geometry = new THREE.Geometry();
  material = new THREE.MeshLambertMaterial( { color: body_part_color[i], transparent: true, opacity:0} );
  body_part_ideal[i].object = new THREE.Line( geometry, material );
  
  body_part[i].setId(i);  
  body_part_ideal[i].setId(i);

  body_node[i] = Object.assign({}, bodyNodeObj );
  body_node[i].object = new THREE.Group();
  body_node[i].setId(i);
  body_node_ideal[i] = Object.assign({}, bodyNodeObj );
  body_node_ideal[i].setId(i);
}

init();
animate();

function init() {
 //-------create body part
  createBodyParts();

 //-------create 3D image
 
  container = document.createElement( 'div' );
  document.body.appendChild( container );
 /* --------for debug
  var info = document.createElement( 'div' );
  info.style.position = 'absolute';
  info.style.top = '10vh';
  info.style.width = '100%';
  info.style.textAlign = 'center';
  info.innerHTML = "Body.setPosition({1,1,1})";
  //info.innerHTML = 'Drag to spin';
  container.appendChild( info );    //for debug
 */
 //-------setup view point 
  camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 1, 2000 );
  camera.position.set( camera_dis, camera_y_position, 0 );

  camera.lookAt(new THREE.Vector3(0, 0, 0));
  scene = new THREE.Scene();
  scene.background = new THREE.Color( 0x444444 );
  scene.position.set(0, 1, 0);
  scene.add( new THREE.AmbientLight( 0xe8e8e8, 1 ) );

  group = new THREE.Group();
  group.position.x = 0;
  scene.add( group );
  person = new THREE.Group();
  group.add( person );
  
 //------spot light
  dir_light = new THREE.DirectionalLight( 0xffffff, 1 );
  dir_light.position.set( 300, 300, -240);
  dir_light.castShadow = true;
  dir_light.shadow.mapSize.width = 1024;  // default
  dir_light.shadow.mapSize.height = 1024; // default
  dir_light.shadow.camera.left = -50;
  dir_light.shadow.camera.right = 50;
  dir_light.shadow.camera.top = 20;
  dir_light.shadow.camera.bottom = -100;
  dir_light.intensity = 1;
  scene.add( dir_light );

  for (var i = 0; i < body_part_number; i++) {
    person.add(body_part[i].object);
    person.add(body_node[i].object);
    person.add(body_part_ideal[i].object);
    person.add(body_node_ideal[i].object);
  }

 //setup renderer, used to draw the 3D images
  renderer = new THREE.WebGLRenderer( { antialias: true } );
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.PCFSoftShadowMap;
  renderer.shadowMapSoft = false;
  container.appendChild( renderer.domElement );

  // ------test code start

  //-------test code end

  container.addEventListener( 'mousedown', onDocumentMouseDown, false );
  container.addEventListener( 'touchstart', onDocumentTouchStart, false );
  container.addEventListener( 'touchmove', onDocumentTouchMove, false );
  container.addEventListener('mousewheel', Chrome_Mouse, false);//Chrome mouse wheel
  //
  window.addEventListener( 'resize', onWindowResize, false );
}

function createBodyParts() {

  for (var i=0; i<body_part_number; i++) {
    if (i==0) {
      geometry = new THREE.SphereGeometry( lowdis, 50, 50 );
      material = new THREE.MeshLambertMaterial( { color: body_part_color[i], transparent: true} );
    }
    else if (i==4 || i==7) {
      geometry = new THREE.SphereGeometry( lowdis/2.5, 50, 50 );
      material = new THREE.MeshLambertMaterial( { color: body_part_color[i], transparent: true} );
    }
    else {
      geometry = new THREE.SphereGeometry( lowdis/3, 50, 50 );
      material = new THREE.MeshLambertMaterial( { color: 0xffffff, transparent: true} );
    }
    var tmp = new THREE.Mesh( geometry, material );
    body_node[i].object.add( tmp );

    if (i==0) {
      geometry = new THREE.SphereGeometry( lowdis, 50, 50 );
      material = new THREE.MeshLambertMaterial( { color: body_part_color[i], transparent: true} );
    }
    else if (i==4 || i==7) {
      geometry = new THREE.SphereGeometry( lowdis/2.5, 50, 50 );
      material = new THREE.MeshLambertMaterial( { color: body_part_color[i], transparent: true} );
    }
    else {
      geometry = new THREE.SphereGeometry( lowdis/3, 50, 50 );
      material = new THREE.MeshLambertMaterial( { color: 0xffffff, transparent: true} );
    }
    tmp = new THREE.Mesh( geometry, material );
    tmp.material.opacity = 0;
    body_node_ideal[i].object = tmp ;

    body_node[i].setPosition( data[0][3*i], data[0][3*i+1], data[0][3*i+2] );
  }

  /* for 3d object
  for(var i=0; i<body_part_number; i++) {
    if (i==1) {
      body_part[i].top_rad = calLength(data[0][3], data[0][4], data[0][5], data[0][6], data[0][7], data[0][8];//neck to right shoulder
      body_part[i].low_rad = lowdis;
    }
    else if (i==2 || i==5) {
      body_part[i].top_rad = lowdis/2;
      body_part[i].low_rad = lowdis/2;
    }
    else if (i==3 || i==6) {
      body_part[i].top_rad = lowdis/2;
      body_part[i].low_rad = lowdis*0.3;
    }
    else if (i==8 || i==11) {
      body_part[i].top_rad = lowdis/2.5;
      body_part[i].low_rad = lowdis/3;
    }
    else if (i==9 || i==12) {
      body_part[i].top_rad = lowdis/3;
      body_part[i].low_rad = lowdis/3.5;
    }
    
    if (i!=4 && i!=7 && i!=10 && i!=13) {
      body_part[i].vec = 1;
    }
  }
  */

  for(var i=0; i<body_part_number; i++) {
    if (i!=4 && i!=7 && i!=10 && i!=13) {
      body_part[i].vec = 1;
      body_part_ideal[i].vec = 1;
      connectIdeal(body_part_ideal[i], 0,0,0,0,0,0)
    }
  }

 //----------------------------------------------------head=0
  body_part[0].p1 = 0;
  body_part[0].p2 = 1;


 //----------------------------------------------------body=1
  body_part[1].p1 = 1;
  body_part[1].p2 = -1;

 //----------------------------------------------------rArm=2
  body_part[2].p1 = 2;
  body_part[2].p2 = 3;
 //----------------------------------------------------rForearm=3
  body_part[3].p1 = 3;
  body_part[3].p2 = 4;

 //----------------------------------------------------rHand=4
 //----------------------------------------------------lArm=5
  body_part[5].p1 = 5;
  body_part[5].p2 = 6;
 //----------------------------------------------------lForearm=6
  body_part[6].p1 = 6;
  body_part[6].p2 = 7;
 //----------------------------------------------------lHand=7
 //----------------------------------------------------rThigh=8
  body_part[8].p1 = 8;
  body_part[8].p2 = 9;
 //----------------------------------------------------rCalf=9
  body_part[9].p1 = 9;
  body_part[9].p2 = 10;
 //----------------------------------------------------lThigh=11
  body_part[11].p1 = 11;
  body_part[11].p2 = 12;
 //----------------------------------------------------lCalf=12
  body_part[12].p1 = 12;
  body_part[12].p2 = 13;

  for (var i=0; i<body_part_number; i++) {
    connect(body_part[i], 0);
  }

 //----------------------------------------------------rFoot=10
  var tmpgroup = new THREE.Group();
  geometry = new THREE.CubeGeometry( lowdis*1.6, lowdis*0.8, lowdis*0.88);
  material = new THREE.MeshLambertMaterial( { color: body_part_color[10], transparent: true} );
  tmp = new THREE.Mesh( geometry, material );
  tmp.position.x = lowdis*0.2;
  tmp.position.y = 0;
  tmpgroup.add( tmp );

  geometry = new THREE.CylinderGeometry( lowdis*0.8, lowdis*0.8, lowdis*0.8, 50, 20, false, 0, Math.PI/2);
  material = new THREE.MeshLambertMaterial( { color: body_part_color[10], transparent: true} );
  tmp = new THREE.Mesh( geometry, material );
  tmp.position.x = lowdis*0.7;
  tmp.rotation.x = -Math.PI/2;
  tmp.position.y = -lowdis*0.4;
  
  tmpgroup.add( tmp );
  tmpgroup.rotation.y = -Math.PI/4;
  tmpgroup.position.y = -lowdis*0.25;
  body_node[10].object.add( tmpgroup );
  var i = 10;
  body_node[i].setPosition( data[0][3*i], data[0][3*i+1],  data[0][3*i+2] );

  
 //----------------------------------------------------lFoot=13

  var tmpgroup = new THREE.Group();
  geometry = new THREE.CubeGeometry( lowdis*1.6, lowdis*0.8, lowdis*0.8);
  material = new THREE.MeshLambertMaterial( { color: body_part_color[13], transparent: true} );

  tmp = new THREE.Mesh( geometry, material );
  tmp.position.x = lowdis*0.2;
  tmp.position.y = 0;//-8
  tmpgroup.add( tmp );

  geometry = new THREE.CylinderGeometry( lowdis*0.8, lowdis*0.8, lowdis*0.8, 50, 20, false, 0, Math.PI/2);
  material = new THREE.MeshLambertMaterial( { color: body_part_color[13], transparent: true} );
  var tmp = new THREE.Mesh( geometry, material );
  tmp.position.x = lowdis*0.7;
  tmp.rotation.x = -Math.PI/2;
  tmp.position.y = -lowdis*0.4;

  tmpgroup.add( tmp );

  tmpgroup.rotation.y = -Math.PI/8;
  tmpgroup.position.y = -lowdis*0.25;

  body_node[13].object.add( tmpgroup );
  var i = 13;
  body_node[i].setPosition( data[0][3*i], data[0][3*i+1], data[0][3*i+2] );
  
}

function connect(part, frame) {

  part.object.geometry.vertices = [];
  var p1 = new THREE.Vector3(  data[frame][3*part.p1],
                               data[frame][3*part.p1+2],
                              -data[frame][3*part.p1+1] );
  var p2 = new THREE.Vector3(  data[frame][3*part.p2],
                               data[frame][3*part.p2+2],
                              -data[frame][3*part.p2+1] );
  if (part.p2 == -1) {
    part.object.geometry.vertices.push(p1);
    p2 = new THREE.Vector3(  data[frame][6], data[frame][6+2], -data[frame][6+1] );
    part.object.geometry.vertices.push(p2);
    p2 = new THREE.Vector3(  data[frame][24], data[frame][24+2], -data[frame][24+1] );
    part.object.geometry.vertices.push(p2);
    p2 = new THREE.Vector3(  data[frame][33], data[frame][33+2], -data[frame][33+1] );
    part.object.geometry.vertices.push(p2);
    p2 = new THREE.Vector3(  data[frame][15], data[frame][15+2], -data[frame][15+1] );
    part.object.geometry.vertices.push(p2);
    part.object.geometry.vertices.push(p1);
    
  }
  else {
    part.object.geometry.vertices.push(p1);
    part.object.geometry.vertices.push(p2);

  }
  part.object.geometry.verticesNeedUpdate = true;
}

function calLength (start_x, start_y, start_z=0, end_x=0, end_y=0, end_z=0) {
  var answer;
  var x_diff = start_x - end_x;
  var y_diff = start_y - end_y;
  var z_diff = start_z - end_z;
  answer = Math.sqrt( Math.pow(x_diff, 2) + Math.pow(y_diff, 2) + Math.pow(z_diff, 2));
  return answer;
}

//---------setup the animation for spin
function onWindowResize() {
  windowHalfX = window.innerWidth / 2;
    //var zoom_ratio = angle_from_center / (angle_unit*0.5);
    //object[front_stele].scale.set( 1.8 - 0.8*zoom_ratio, 1.8 - 0.8*zoom_ratio, 1.8 - 0.8*zoom_ratio );
  windowHalfY = window.innerHeight / 2;
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize( window.innerWidth, window.innerHeight );
}
function onDocumentMouseDown( event ) {
  event.preventDefault();
  target_rotation.x = group.rotation.x;
  target_rotation.y = group.rotation.y;
  document.addEventListener( 'mousemove', onDocumentMouseMove, false );
  document.addEventListener( 'mouseup', onDocumentMouseUp, false );
  document.addEventListener( 'mouseout', onDocumentMouseOut, false );
  mouse_mouse_down.x = event.clientX - windowHalfX;
  mouse_mouse_down.y = -event.clientY + windowHalfY;
  target_rotation_on_mouse_down.x = target_rotation.x;
  target_rotation_on_mouse_down.y = target_rotation.y;
}
function onDocumentMouseMove( event ) {
  mouse_mouse_up.x = event.clientX - windowHalfX;
  mouse_mouse_up.y = -event.clientY + windowHalfY;
  target_rotation.y = target_rotation_on_mouse_down.y + ( mouse_mouse_up.x - mouse_mouse_down.x ) * 0.008;
  target_rotation.x = target_rotation_on_mouse_down.x + ( mouse_mouse_up.y - mouse_mouse_down.y ) * 0.008;
}
function onDocumentMouseUp( event ) {
  document.removeEventListener( 'mousemove', onDocumentMouseMove, false );
  document.removeEventListener( 'mouseup', onDocumentMouseUp, false );
  document.removeEventListener( 'mouseout', onDocumentMouseOut, false );
}
function onDocumentMouseOut( event ) {
  document.removeEventListener( 'mousemove', onDocumentMouseMove, false );
  document.removeEventListener( 'mouseup', onDocumentMouseUp, false );
  document.removeEventListener( 'mouseout', onDocumentMouseOut, false );
}
function onDocumentTouchStart( event ) {

  target_rotation.x = group.rotation.x;
  target_rotation.y = group.rotation.y;
  if ( event.touches.length == 1 ) {
    event.preventDefault();
    mouse_mouse_down.x = event.touches[ 0 ].pageX - windowHalfX;
    target_rotation_on_mouse_down.x = target_rotation.x;
    target_rotation_on_mouse_down.y = target_rotation.y;
  }
  else {
    target_rotation.x = group.rotation.x;    
    target_rotation.y = group.rotation.y;
  }
}
function onDocumentTouchMove( event ) {
  if ( event.touches.length == 1 ) {
    event.preventDefault();
    mouse_mouse_up.x = event.touches[ 0 ].pageX - windowHalfX;
    mouse_mouse_up.y = event.touches[ 0 ].pageY - windowHalfY;
    target_rotation.x = target_rotation_on_mouse_down.x + ( mouse_mouse_up.x - mouse_mouse_down.x ) * 0.07;
    target_rotation.y = target_rotation_on_mouse_down.y + ( mouse_mouse_up.y - mouse_mouse_down.y ) * 0.07;
  }
}
function Chrome_Mouse(evt) { 
  //prevent tthe scroll on web 
  if(evt.preventDefault) evt.preventDefault(); 
  else evt.returnValue = false;  
  
  if(evt.wheelDelta > 0)  //(Chrome) 
    camera_dis++; //Zoom in         
  else 
    camera_dis--;//Zoom out
  camera.position.set( camera_dis, camera_y_position, 0 );
  camera.lookAt(new THREE.Vector3(0, 0, 0));
}

function animate() {
  requestAnimationFrame( animate );
  render();
}

function render() { //3D rotation function
  group.rotation.z += ( target_rotation.x - group.rotation.x ) * 0.008;
  group.rotation.y += ( target_rotation.y - group.rotation.y ) * 0.008;
  renderer.render( scene, camera );
}

function runFrame( num , frame_delay=0) {
  play_animation[num] = setTimeout(function() {
    for(var i = 0; i < body_part_number; i++) {
      if (body_part[i].enable) {
        if ( i!=4 && i!=7 && i!=10 && i!=13) {
          connect(body_part[i], num);
        }
      }
      if (body_node[i].enable) {
        body_node[i].setPosition( data[num][3*i], data[num][3*i+1], data[num][3*i+2]);
      }

    }
    present++;
    if (present == data['frame']) {
      present = 0;
      document.getElementById('playstop').innerHTML = "Start";
      play = 1;
    }
    if (adjust_data!=null) 
      runRehab(num);

    //console.log(data['filename']);
  }, frame_delay);
}


function runRehab(num) {
  
    for (var i = adjust_data['adjust_node'].length - 1; i >= 0; i--) {
      var part = adjust_data['adjust_node'][i];

      body_node_ideal[part].setPosition(adjust_data[num][3*i], adjust_data[num][3*i+1], adjust_data[num][3*i+2]);
    }
    for (var i = adjust_data['adjust_part'].length - 1; i >= 0; i--) {
      var node = adjust_data['adjust_part'][i][0]
      var p1 = adjust_data['adjust_part'][i][1]
      var p2 = adjust_data['adjust_part'][i][2]
      for (var j = adjust_data['adjust_node'].length - 1; j >= 0; j--) {
        if (p1 == adjust_data['adjust_node'][j]) {
          p1 = j;
          break;
        }
      }
      for (var j = adjust_data['adjust_node'].length - 1; j >= 0; j--) {
        if (p2 == adjust_data['adjust_node'][j]) {
          p2 = j;
          break;
        }
      }
      connectIdeal( body_part_ideal[node],adjust_data[num][3*p1], adjust_data[num][3*p1+1], adjust_data[num][3*p1+2], adjust_data[num][3*p2], adjust_data[num][3*p2+1], adjust_data[num][3*p2+2]);
    }
    document.getElementById('Count').innerHTML = ("Count: " + adjust_data['count'][num]);
    document.getElementById('Accurate').innerHTML = ("Accurate: " + adjust_data['accurate'][num] + "%");

}

//----play button
var play = 1; //ready to play the animation
var present = 0;  //the frame num recently played
$(document).on('touchend click', '#playstop', function() {
  event.preventDefault();
  if (play == 1) {
    for(var i = present; i < data.frame; i++ ){
      clearTimeout(play_animation[i]);
      var frame_delay = (i-present) * time_delay
      runFrame(i, frame_delay);
    }
    this.innerHTML = "Stop";
    play = 0;
  }
  else {
    for(var i = 0; i < data.frame; i++ ){
      clearTimeout(play_animation[i]);
    }
    play = 1;
    this.innerHTML = "Start";
  }
})

// show the body part or not
var count_disable = 0;
$(".enable").click(function () {
  var node = this.value;
  var con = node -1;
  if (node == 1 || con<0) {
    con++;
  }
  if (this.value == 14) { //show everything
    $(".enable").prop("checked", true);
    $("#nothing").prop("checked", false);

    for (var i = 13; i >= 0; i--) {
      body_node[i].enable = true;
      body_part[i].enable = true;
      if ( i!=4 && i!=7 && i!=10 && i!=13) {
        body_part[i].object.material.opacity = true;
      }

      body_node[i].object.children[0].material.opacity = true;
      //body_node[node].object.children[0].material.transparent=!this.checked;
      if (i==10 || i==13) {
        for (var j = body_node[i].object.children[1].children.length - 1; j >= 0; j--) {
          body_node[i].object.children[1].children[j].material.opacity = true;
        }
      }
    }
  }
  else if (this.value == 15) { //disable everything
    $(".enable").prop("checked", false);
    $("#nothing").prop("checked", true);
    for (var i = 13; i >= 0; i--) {
      body_node[i].enable = false;
      body_part[i].enable = false;
      if ( i!=4 && i!=7 && i!=10 && i!=13) {
        body_part[i].object.material.opacity = false;
      }

      body_node[i].object.children[0].material.opacity = false;
      //body_node[node].object.children[0].material.transparent=!this.checked;
      if (i==10 || i==13) {
        for (var j = body_node[i].object.children[1].children.length - 1; j >= 0; j--) {
          body_node[i].object.children[1].children[j].material.opacity = false;
        }
      }
    }
  }
  else {
    body_node[node].enable = this.checked;
    body_part[con].enable = this.checked;
    if (!this.checked) 
      count_disable++;
    else
      count_disable--;

    if (count_disable!= 0) 
      $("#wholebody").prop("checked", false);
    else 
      $("#wholebody").prop("checked", true);

    if ( node!=2 && con!=4 && con!=7 && con!=10 && con!=13) {
      body_part[con].object.material.opacity = this.checked;
    }

    body_node[node].object.children[0].material.opacity = this.checked;
    //body_node[node].object.children[0].material.transparent=!this.checked;
    if (node==10 || node==13) {
      for (var j = body_node[node].object.children[1].children.length - 1; j >= 0; j--) {
        body_node[node].object.children[1].children[j].material.opacity = this.checked;
      }
    }
  }
})

var treatment_choice = [0,0,0,0];
var correct_move = 0;
var pose_reset = true;
var adjust_data = null;

/*
$(".rehabPos").click(function () {
  correct_move = 0
  $(".rehabPos").not(this).prop("checked", false);

  if (this.checked == true) {
    var choice = this.value;
    calRehab(choice);
  }
  else {
    calRehab(-1);
  }
})
*/

function calRehab(choice) {
  $.ajax({
    method: 'POST',
    data: {
      data_name: data['filename'],
      choice: choice
    },
    url: '/calRehab/',
    success: function(data) {
      adjust_data = data;
      if (adjust_data!=null)
        setRehab(adjust_data);
    }
  })
  resetIdeal();
}

function setRehab(data) {
  for (var i = data['adjust_node'].length - 1; i >= 0; i--) {
    body_node_ideal[data['adjust_node'][i]].object.material.opacity = 0.5;
  }
  for (var i = data['adjust_part'].length - 1; i >= 0; i--) {
    body_part_ideal[data['adjust_part'][i][0]].object.material.opacity = 0.5;
  }
}

function resetIdeal() {
  for (var i = body_part_ideal.length - 1; i >= 0; i--) {
    body_part_ideal[i].object.material.opacity = 0;
    body_node_ideal[i].object.material.opacity = 0;
  }
}


//input two vector
function calAngle(v1_x, v1_y, v1_z=0, v2_x=0, v2_y=0, v2_z=0) {
  var answer = [0,0,0];
  var dot_product = v1_x*v2_x + v1_y*v2_y + v1_z*v2_z;
  var v1_len = calLength(v1_x, v1_y, v1_z);
  var v2_len = calLength(v2_x, v2_y, v2_z);
  var cosine_theta = dot_product/v1_len/v2_len;
  var theta = Math.acos(cosine_theta);
  answer[0] = theta;
  answer[1] = v1_len;
  answer[2] = v2_len;
  return answer;
}


function connectIdeal(part, p1_x, p1_y, p1_z, p2_x, p2_y, p2_z) {
  part.object.geometry.vertices = [];
  var p1 = new THREE.Vector3(  p1_x, p1_z, -p1_y );
  var p2 = new THREE.Vector3(  p2_x, p2_z, -p2_y );
  part.object.geometry.vertices.push(p1);
  part.object.geometry.vertices.push(p2);
  part.object.geometry.verticesNeedUpdate = true;

}

function crossProduct(v1_x, v1_y, v1_z, v2_x, v2_y, v2_z) {
  var answer = [0,0,0];
  answer[0] = v1_y*v2_z - v1_z*v2_y;
  answer[1] = v1_z*v2_x - v1_x*v2_z;
  answer[2] = v1_x*v2_y - v1_y*v2_x;
  return answer;
}


//--- Slider
var frameSlider = document.getElementById("frameSpeed");
var frameOutput = document.getElementById("frameSpeedValue");
frameOutput.innerHTML = frameSlider.value;
var camSlider = document.getElementById("cameraDis");
var camOutput = document.getElementById("cameraDisValue");
camOutput.innerHTML = (camSlider.value/1.5).toFixed(2);

frameSlider.oninput = function() {
  frameOutput.innerHTML = this.value;
  time_delay = this.value;
  // stop and replay
  if(!play) {
    for(var i = present; i < data.frame; i++ ){
      clearTimeout(play_animation[i]);
      var frame_delay = (i-present) * time_delay
      runFrame(i, frame_delay);
    }
  }
}

camSlider.oninput = function() {
  camOutput.innerHTML = (this.value/1.5).toFixed(2);
  camera_dis = this.value;
  camera.position.set( camera_dis, camera_y_position, 0 );
  camera.lookAt(new THREE.Vector3(0, 0, 0));
}


// data list
for (var i = 0; i < data_list.length; i++) {
  var info = document.createElement( 'option' );
  info.innerHTML = data_list[i].slice(0, -5);
  info.classList.add("rehabPos");
  info.classList.add("words1");
  info.value=i;
  document.getElementById("dataList").appendChild( info );    //for debug
}

function getChooseData(choice) {
  $.ajax({
    method: 'GET',
    data: {
      data_name: data_list[choice]
    },
    url: '/getChooseData/',
    success: function(data_tmp) {
      data = data_tmp;
      present = 0;
    }
  })
}
