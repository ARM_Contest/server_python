import shutil
import os
import cv2
from PIL import Image

def clearFolder():
	shutil.rmtree('/home/mrzhou/Pictures/data_image/first_image')
	shutil.rmtree('/home/mrzhou/Pictures/data_image/second_image')
	os.makedirs('/home/mrzhou/Pictures/data_image/first_image')
	os.makedirs('/home/mrzhou/Pictures/data_image/second_image')

	# shutil.rmtree('/home/mrzhou/Pictures/data_image/first_image_test')
	# shutil.rmtree('/home/mrzhou/Pictures/data_image/second_image_test')
	# os.makedirs('/home/mrzhou/Pictures/data_image/first_image_test')
	# os.makedirs('/home/mrzhou/Pictures/data_image/second_image_test')
	pass

def videoToImage(left_video, right_video):
	clearFolder()
	left_vid_cap = cv2.VideoCapture(left_video)
	right_vid_cap = cv2.VideoCapture(right_video)
	l_success = 1
	r_success = 1
	count = 0
	l_image = 0
	r_image = 0
	r_success,r_image = right_vid_cap.read()
	l_success,l_image = left_vid_cap.read()
	while r_success and l_success:
		# 30 /2
		cv2.imwrite("/home/mrzhou/Pictures/data_image/first_image/Left%04d.jpg" % count, l_image)
		cv2.imwrite("/home/mrzhou/Pictures/data_image/second_image/Right%04d.jpg" % count, r_image)


		# cv2.imwrite("/home/mrzhou/Pictures/data_image/first_image_test/Left%04d.jpg" % count, l_image)
		# cv2.imwrite("/home/mrzhou/Pictures/data_image/second_image_test/Right%04d.jpg" % count, r_image)

		# im = Image.open("/home/mrzhou/Pictures/data_image/first_image/Left%04d.jpg" % count)
		# im = im.transpose(Image.ROTATE_270)
		# im.save("/home/mrzhou/Pictures/data_image/first_image/Left%04d.jpg" % count)

		# im = Image.open("/home/mrzhou/Pictures/data_image/second_image/Right%04d.jpg" % count)
		# im = im.transpose(Image.ROTATE_270)
		# im.save("/home/mrzhou/Pictures/data_image/second_image/Right%04d.jpg" % count)

		# im = Image.open("/home/mrzhou/Pictures/data_image/first_image_test/Left%04d.jpg" % count)
		# im = im.transpose(Image.ROTATE_270)
		# im.save("/home/mrzhou/Pictures/data_image/first_image_test/Left%04d.jpg" % count)

		# im = Image.open("/home/mrzhou/Pictures/data_image/second_image_test/Right%04d.jpg" % count)
		# im = im.transpose(Image.ROTATE_270)
		# im.save("/home/mrzhou/Pictures/data_image/second_image_test/Right%04d.jpg" % count)


		r_success,r_image = right_vid_cap.read()
		l_success,l_image = left_vid_cap.read()
		# r_success,r_image = right_vid_cap.read()
		# l_success,l_image = left_vid_cap.read()
		count = count+1
		#print(count)
		pass

	pass
if __name__ == '__main__':
    videoToImage("/home/mrzhou/Pictures/IMG_2921.mov", '/home/mrzhou/Pictures/IMG_2918.mov')
